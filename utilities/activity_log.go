package utilities

import "gitlab.com/tuneverse/toolkit/models"

// NewActivityLog creates a new ActivityLog object with the provided member ID, action, and data and returns a newly created ActivityLog
// instance with the specified values.
// It is a convenient function to initialize an ActivityLog instance.
// @ params memberID: The unique identifier of the member associated with the activity.
// @ params- action: The type of activity or action being logged.
// @ params- data: Additional data associated with the activity in a map format.
func NewActivityLog(memberID, action string, data map[string]interface{}) models.ActivityLog {
	return models.ActivityLog{
		MemberID: memberID,
		Action:   action,
		Data:     data,
	}
}
