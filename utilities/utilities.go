package utilities

import (
	"fmt"
	"net/http"
	"path/filepath"
	"reflect"
	"regexp"
	"store/internal/consts"
	"store/internal/entities"
	"strconv"
	"strings"

	"github.com/labstack/gommon/log"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/models/api"
)

type errorMap map[string]models.ErrorResponse

func NewErrorMap() errorMap {
	return make(errorMap)
}

// PreparePlaceholders prepares placeholders for SQL queries.
func PreparePlaceholders(n int) string {
	var b strings.Builder
	for i := 0; i < n; i++ {
		b.WriteString(fmt.Sprintf("$%d,", i+1))
	}
	return strings.TrimSuffix(b.String(), ",")
}

func Contains(slice []string, element string) bool {
	for _, e := range slice {
		if e == element {
			return true
		}
	}
	return false
}

// CalculateOffset builds a pagination query offset based on the given page and limit.
func CalculateOffset(page, limit int) string {
	if page <= 0 {
		page = consts.DefaultPage
	}
	if limit <= 0 || limit > consts.LimitVal {
		limit = consts.DefaultLimit
	}
	offset := fmt.Sprintf("%s %d %s %d", "LIMIT", limit, "OFFSET", (page-1)*limit)
	return offset
}

// MetaDataInfo calculates values for pagination.
func MetaDataInfo(metaData entities.MetaData) entities.MetaData {
	if int64(metaData.CurrentPage)*int64(metaData.PerPage) < metaData.Total {
		metaData.Next = metaData.CurrentPage + 1
	}
	if metaData.CurrentPage > 1 {
		metaData.Prev = metaData.CurrentPage - 1
	}
	return metaData
}

func GenerateValidationError(invalidKeys []string, message string, code int) api.Response {
	errorMap := api.Response{}
	keyErrors := make(map[string][]string)

	// Populating the keyErrors map
	for _, key := range invalidKeys {
		keyErrors[key] = append(keyErrors[key], "Invalid key "+key)
	}

	// Constructing the error response struct
	errorMap.Status = "failure"
	errorMap.Message = "Validation error"
	errorMap.Code = 400
	errorMap.Data = map[string]interface{}{}
	errorMap.Errors = map[string]interface{}{
		"errors": keyErrors,
	}
	return errorMap
}

func GenerateTypeValidationError(keys []string, data map[string]interface{}) api.Response {
	errors := make(map[string]string)
	for _, key := range keys {
		value := data[key]
		errors[key] = fmt.Sprintf("Field %s must be of type %s", key, reflect.TypeOf(value))
	}
	return api.Response{
		Status:  "failure",
		Message: "Validation error",
		Code:    http.StatusBadRequest,
		Data:    struct{}{},
		Errors:  errors,
	}
}

func IsValidType(value interface{}, expectedType reflect.Type) bool {
	if value == nil {
		return false
	}
	// Check if value's type matches the expected type
	return reflect.TypeOf(value) == expectedType
}

func ContainsSpecialChars(input string) bool {
	pattern := `[!@#$%^&*()+\=[\]{};':"\\|,.<>?]`
	regex, err := regexp.Compile(pattern)
	if err != nil {
		fmt.Println("Error compiling regular expression:", err)
		return false
	}
	match := regex.FindString(input)
	return match != ""
}

func IsValidPath(path string) bool {
	isAbs := filepath.IsAbs(path)
	if isAbs {
		if strings.Contains(path, " ") {
			return false
		}
	}
	return isAbs
}

func Int64ToStringConvert(Ids []int64) (string, error) {
	idStrings := make([]string, len(Ids))
	for i, id := range Ids {
		idStrings[i] = strconv.FormatInt(id, 10)
	}
	return strings.Join(idStrings, ","), nil
}

func ValidateDataTypes(data map[string]interface{}, validKeysType reflect.Type, helpLink string) map[string][]entities.ErrorItem {
	invalidKeys := make(map[string][]entities.ErrorItem)
	for key := range data {
		found := false
		key = strings.ToLower(key)
		for i := 0; i < validKeysType.NumField(); i++ {
			field := validKeysType.Field(i)
			validKey := strings.ToLower(field.Tag.Get("json"))
			if key == validKey {
				found = true
				expectedType := field.Type.Kind().String()
				actualValue := reflect.ValueOf(data[key])
				actualType := actualValue.Kind().String()
				// expectedType := field.Type.String()
				// actualValue := reflect.ValueOf(data[key])
				// actualType := actualValue.Type().String()
				if actualType != expectedType {
					if expectedType == "bool" {
						expectedType = "boolean"
					}
					log.Errorf("Invalid datatype, Field '%s' must be %s", key, expectedType)
					invalidKeys[key] = append(invalidKeys[key], entities.ErrorItem{
						ErrorCode: "A1019",
						Field:     key,
						Help:      helpLink,
						Message:   []string{fmt.Sprintf("Field '%s' must be %s value", key, expectedType)},
					})
				}
				break
			}
		}
		if !found {
			invalidKeys[key] = append(invalidKeys[key], entities.ErrorItem{
				ErrorCode: "A1019",
				Field:     key,
				Help:      helpLink,
				Message:   []string{fmt.Sprintf("Invalid key %s", key)},
			})
		}
	}
	return invalidKeys
}

func ConvertToStrings(slice []string) string {
	return "'" + strings.Join(slice, "','") + "'"
}
