package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	serviceconfig "store/config"
	"store/internal/consts"
	"store/internal/controllers"
	"store/internal/entities"
	"store/internal/middlewares"
	"store/internal/repo"
	"store/internal/repo/driver"
	"store/internal/usecases"
	"syscall"
	"time"

	"gitlab.com/tuneverse/toolkit/core/activitylog"
	"gitlab.com/tuneverse/toolkit/core/cloud/awsutils"

	"github.com/aws/aws-sdk-go-v2/config"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/patrickmn/go-cache"
	"gitlab.com/tuneverse/toolkit/core/awsmanager"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/middleware"
	"gitlab.com/tuneverse/toolkit/utils"
)

// method run
// env configuration
// logrus, zap
// use case intia
// repo initalization
// controller init

// Run starts the application and listens for incoming HTTP requests.
func Run() {
	// init the env config
	cfg, err := serviceconfig.LoadConfig(consts.AppName)
	if err != nil {
		panic(err)
	}

	// logrus init
	// Create a file logger configuration with specified settings.
	file := &logger.FileMode{
		LogfileName:  "stores.log",     // Log file name.
		LogPath:      "logs",           // Log file path. Add this folder to .gitignore as logs/
		LogMaxAge:    7,                // Maximum log file age in days.
		LogMaxSize:   1024 * 1024 * 10, // Maximum log file size (10 MB).
		LogMaxBackup: 5,                // Maximum number of log file backups to keep.
	}

	// ################ For Logging to a Database ################
	// Create a database logger configuration with the specified URL and secret.
	// For development purpose don't use cloudMode.

	// ############# Client Options #############
	// Configure client options for the logger.
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  true,           // Include request data in logs.
		IncludeResponseDump: true,           // Include response data in logs.
	}

	var llog *logger.Logger
	// Check if the application is in debug mode.
	if cfg.Debug {
		llog = logger.InitLogger(clientOpt, file)
	} else {
		db := &logger.CloudMode{

			URL:    cfg.LoggerServiceURL,
			Secret: cfg.LoggerSecret,
		}
		llog = logger.InitLogger(clientOpt, db, file)
	}

	// activity log implementation
	activitylog, err := activitylog.Init(cfg.ActivityLogServiceURL)
	if err != nil {
		log.Fatalf("unable to connect the activity log service : %v", err)
		return
	}

	// database connection
	pgsqlDB, err := driver.ConnectDB(cfg.Db)
	if err != nil {
		llog.Fatalf("unable to connect the database")
		return
	}

	awsConfig, err := awsInit(cfg)
	if err != nil {
		log.Fatalf("Failed to initialize aws, err=%s", err.Error())
		return
	}

	// here initalizing the router
	router := initRouter()
	if !cfg.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	api := router.Group("/api")

	m := middlewares.NewMiddlewares(cfg)
	api.Use(m.PartnerID())

	// api.Use(m.LocalizationLanguage())
	// api.Use(m.EndPointNames())

	// middleware initialization
	api.Use(middleware.APIVersionGuard(middleware.VersionOptions{
		AcceptedVersions: cfg.AcceptedVersions,
	}))
	api.Use(middleware.LogMiddleware(map[string]interface{}{}))
	api.Use(middleware.Localize())
	api.Use(middleware.ErrorLocalization(
		middleware.ErrorLocaleOptions{
			Cache:                  cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:        time.Duration(time.Hour * 24),
			CacheKeyLabel:          "ERROR_CACHE_KEY_LABEL",
			LocalisationServiceURL: fmt.Sprintf("%s/localization/error", cfg.LocalisationServiceURL),
		},
	))
	api.Use(middleware.EndpointExtraction(
		middleware.EndPointOptions{
			Cache:            cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:  time.Duration(time.Hour * 24),
			CacheKeyLabel:    consts.CacheEndpointsKey,
			ContextEndPoints: consts.ContextEndPoints,
			EndPointsURL:     fmt.Sprintf("%s/localization/endpointname", cfg.EndpointURL),
		},
	))
	// complete store related initialization
	{
		cloud := awsutils.NewCloudService(awsConfig)

		// repo initialization
		storeRepo := repo.NewStoreRepo(pgsqlDB)

		// initilizing usecases
		storeUseCases := usecases.NewStoreUseCases(storeRepo, cloud, cfg)

		// initalizing controllers
		storeControllers := controllers.NewStoreController(api, storeUseCases, cfg, activitylog)

		// init the routes
		storeControllers.InitRoutes()
	}

	// run the app
	launch(cfg, router)
}

func initRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode(gin.DebugMode)

	// CORS
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "DELETE", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		// AllowOriginFunc: func(origin string) bool {
		// },
		MaxAge: 12 * time.Hour,
	}))

	// common middlewares should be added here

	return router
}

// launch
func launch(cfg *entities.EnvConfig, router *gin.Engine) {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	fmt.Println("Server listening in...", cfg.Port)
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	// kill (no param) default send syscanll.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall. SIGKILL but can"t be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	// catching ctx.Done(). timeout of 5 seconds.
	select {
	case <-ctx.Done():
		log.Println("timeout of 5 seconds.")
	}
	log.Println("Server exiting")
}

func awsInit(cfg *entities.EnvConfig) (*awsmanager.AwsConfig, error) {
	var optFns []func(*config.LoadOptions) error
	if !utils.IsEmpty(cfg.AWS.AccessKey) && !utils.IsEmpty(cfg.AWS.AccessSecret) {
		optFns = append(optFns, awsmanager.WithCredentialsProvider(cfg.AWS.AccessKey, cfg.AWS.AccessSecret))
	}
	if !utils.IsEmpty(cfg.AWS.Region) {
		optFns = append(optFns, awsmanager.WithRegion(cfg.AWS.Region))
	}
	awsConf, err := awsmanager.CreateAwsSession(optFns...)
	if err != nil {
		return nil, err
	}
	return awsConf, nil
}
