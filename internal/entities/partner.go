package entities

type PartnerApiResponse struct {
	Status  string                 `json:"status"`
	Message string                 `json:"message"`
	Code    int                    `json:"code"`
	Data    Partner                `json:"data"`
	Errors  map[string]interface{} `json:"errors"`
}

type Partner struct {
	Name                        string                  `json:"name"`
	URL                         string                  `json:"url"`
	Logo                        string                  `json:"logo"`
	Favicon                     string                  `json:"favicon"`
	Loader                      string                  `json:"loader"`
	LoginPageLogo               string                  `json:"login_page_logo"`
	BackgroundColor             string                  `json:"background_color"`
	BackgroundImage             string                  `json:"background_image"`
	Active                      bool                    `json:"active"`
	WebsiteURL                  string                  `json:"website_url"`
	Language                    string                  `json:"language,omitempty"`
	BrowserTitle                string                  `json:"browser_title"`
	ProfileURL                  string                  `json:"profile_url"`
	PaymentURL                  string                  `json:"payment_url"`
	LandingPage                 string                  `json:"landing_page"`
	MobileVerifyInterval        int                     `json:"mobile_verify_interval"`
	TermsAndConditionsVersionID int                     `json:"terms_and_conditions_version_id"`
	PayoutTargetCurrency        string                  `json:"payout_target_currency"`
	ThemeID                     int                     `json:"theme_id"`
	EnableMail                  bool                    `json:"enable_mail"`
	LoginType                   string                  `json:"login_type"`
	BusinessModel               string                  `json:"business_model"`
	MemberPayToPartner          bool                    `json:"member_pay_to_partner"`
	AddressDetails              AddressDetails          `json:"address_details"`
	SubscriptionPlanDetails     SubscriptionPlanDetails `json:"subscription_details,omitempty"`
	MemberGracePeriod           string                  `json:"member_grace_period"`
	ExpiryWarningCount          int                     `json:"expiry_warning_count"`
	AlbumReviewEmail            string                  `json:"album_review_email"`
	SiteInfo                    string                  `json:"site_info"`
	DefaultPriceCode            int                     `json:"default_price_code"`
	DefaultPriceCodeCurrency    Currency                `json:"default_price_code_currency"`
	MusicLanguage               string                  `json:"music_language"`
	MemberDefaultCountry        string                  `json:"member_default_country"`
	OutletsProcessingDuration   int                     `json:"outlets_processing_duration"`
	FreePlanLimit               int                     `json:"free_plan_limit"`
	MemberDefaultCountryID      string                  `json:"member_default_country_id"`
	ProductReview               string                  `json:"product_review"`
}

// AddressDetails struct
type AddressDetails struct {
	CountryID  interface{} `json:"country_id,omitempty"`
	Address    string      `json:"address,omitempty"`
	Street     string      `json:"street,omitempty"`
	Country    string      `json:"country,omitempty"`
	State      string      `json:"state,omitempty"`
	City       string      `json:"city,omitempty"`
	PostalCode string      `json:"postal_code,omitempty"`
}

// SubscriptionPlanDetails struct
type SubscriptionPlanDetails struct {
	ID         string `json:"plan_id,omitempty"`
	StartDate  string `json:"plan_start_date,omitempty"`
	LaunchDate string `json:"plan_launch_date,omitempty"`
}
type Currency struct {
	ID   interface{} `json:"id,omitempty"`
	Name string      `json:"name,omitempty"`
}
