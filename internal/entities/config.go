package entities

// EnvConfig represents the configuration for the environment.
type EnvConfig struct {
	Debug                  bool     `default:"true" split_words:"true"`
	Port                   int      `default:"8080" split_words:"true"`
	Db                     Database `split_words:"true"`
	AcceptedVersions       []string `required:"true" split_words:"true"`
	MigrationPath          string   `split_words:"true"`
	LocalisationServiceURL string   ` split_words:"true"`
	LoggerServiceURL       string   `split_words:"true"`
	LoggerSecret           string   `split_words:"true"`
	ResetLink              string   `split_words:"true"`
	EndpointURL            string   `split_words:"true"`
	AWS                    AWS      `split_words:"true"`
	ErrorHelpLink          string   `split_words:"true"`
	ActivityLogServiceURL  string   `split_words:"true"`
}

// Database represents the database configuration.
type Database struct {
	User     string
	Password string
	Port     int
	Host     string
	DATABASE string
	Schema   string
	// Schema    string `envconfig:"default=public"`
	MaxActive int
	MaxIdle   int
}

type AWS struct {
	AccessKey    string `split_words:"true"`
	AccessSecret string `split_words:"true"`
	Region       string
	BucketName   string `split_words:"true"`
}
