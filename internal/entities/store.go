package entities

import "github.com/google/uuid"

// Store represents a store entity.
type Store struct {
	ID                       string `json:"id"`
	Name                     string `json:"name"`
	ParentID                 string `json:"parent_id"`
	RemotePath               string `json:"remote_path"`
	IsActive                 bool   `json:"active"`
	IsLive                   bool   `json:"live"`
	UseGenericPackage        bool   `json:"use_generic_package"`
	VariableName             string `json:"variable_name"`
	ReportingType            string `json:"reporting_type"`
	FeatureFMName            string `json:"featurefm_name"`
	IsFeatureFMEnabled       bool   `json:"featurefm_enabled" `
	ProcessDurationDays      int    `json:"process_duration_days"`
	DDEXVersionID            string `json:"ddex_version_id"`
	CRSPermissionID          int    `json:"crs_permission_id"`
	IsDolbyEnabled           bool   `json:"dolby_enabled"`
	DolbyCodecID             string `json:"dolby_codec_id"`
	IsStereoRequiredForDolby bool   `json:"stereo_required_for_dolby"`
	IsSonyEnabled            bool   `json:"sony_enabled"`
	PreferredProviderProgram bool   `json:"preferred_provider_program"`
	IncludeFeaturedArtist    bool   `json:"include_featured_artist"`
	IsPriceCodeEnabled       bool   `json:"price_code_enabled"`
	IsPreSaveEnabled         bool   `json:"pre_save_enabled"`
	IsDefault                bool   `json:"default"`
	Logo                     string `json:"logo"`
	IsLocked                 bool   `json:"locked"`
}

type StoreRoles struct {
	ID                    int64     `json:"id"`
	RoleID                uuid.UUID `json:"role_id"`
	RoleName              string    `json:"role"`
	StoreRoleName         string    `json:"store_role"`
	StoreName             string    `json:"store_name"`
	StoreIngestionSpecID  int       `json:"store_ingestion_spec_id"`
	IsUserDefined         bool      `json:"is_user_defined"`
	IsResourceContributor bool      `json:"is_resource_contributor"`
}

type StoreGenres struct {
	ID                   int64     `json:"id"`
	GenreID              uuid.UUID `json:"genre_id"`
	GenreName            string    `json:"genre"`
	StoreGenreName       string    `json:"store_genre"`
	StoreName            string    `json:"store_name"`
	StoreIngestionSpecID int       `json:"store_ingestion_spec_id"`
}

// MetaData represents metadata associated with a store.
type MetaData struct {
	Total       int64 `json:"total"`
	PerPage     int32 `json:"per_page"`
	CurrentPage int32 `json:"current_page"`
	Next        int32 `json:"next"`
	Prev        int32 `json:"prev"`
}

// ResponseMetaData represents metadata associated with a response.
type ResponseMetaData struct {
	MetaData interface{} `json:"meta_data"`
	Data     interface{} `json:"records"`
}

// Response represents a response entity.
type Response struct {
	Data interface{} `json:"data"`
}

// Params represents parameters associated with a request.
type Params struct {
	Q        string
	Page     string
	Limit    string
	Sort     string
	Order    string
	StoreIds []string `json:"store_ids"`
	ShowAll  bool
	Status   string
	IsLive   string `json:"live"`
}

// ErrorResponse represents an error response entity.
type ErrorResponse struct {
	Message   string                 `json:"message"`
	ErrorCode interface{}            `json:"errorCode"`
	Errors    map[string]interface{} `json:"errors"`
}

type KeyValuePair struct {
	Key   string
	Value string
}

type ReqParams struct {
	Page      int32  `form:"page"`
	Limit     int32  `form:"limit"`
	Sort      string `form:"sort"`
	Order     string `form:"order"`
	Status    string `form:"status"`
	Deleted   string `form:"deleted"`
	PartnerId string `form:"partner_id"`
}

type ValidStoreGenreKeys struct {
	StoreGenreName string `json:"store_genre"`
}

type ValidStoreRolesKeys struct {
	StoreRoleName         string `json:"store_role"`
	IsUserDefined         bool   `json:"is_user_defined"`
	IsResourceContributor bool   `json:"is_resource_contributor"`
}

type ErrorItem struct {
	ErrorCode string   `json:"error_code"`
	Field     string   `json:"field"`
	Help      string   `json:"help"`
	Message   []string `json:"message"`
}

type StoreGenre struct {
	GenreID              string `json:"genre_id"`
	StoreIngestionSpecID int    `json:"store_ingestion_spec_id"`
}

type ActiveStoreDetail struct {
	ID   string `json:"store_id"`
	Name string `json:"name"`
}
