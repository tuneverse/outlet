package usecases_test

import (
	"context"
	"database/sql"
	"testing"

	"store/internal/consts"
	"store/internal/entities"
	"store/internal/repo"
	"store/internal/usecases"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/models"
)

func init() {
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName,
		LogLevel:            "info",
		IncludeRequestDump:  false,
		IncludeResponseDump: false,
	}
	logger.InitLogger(clientOpt)
}

func TestStoreUseCases_GetStores(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}
	params := entities.Params{
		Page:  "1",
		Limit: "10",
	}
	t.Run("GetStores", func(t *testing.T) {
		fieldsMap, err := suc.GetStores(ctx, params)
		assert.Error(t, err)
		assert.Empty(t, fieldsMap)
	})
}

func TestCreateStore(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}

	t.Run("InvalidInput", func(t *testing.T) {
		data := map[string]interface{}{
			"name":                       "Student store",
			"parent_id":                  "96b05633-69d7-4144-b0f9-73765b9ea7ca",
			"reporting_type":             "txt",
			"featurefm_enabled":          true,
			"price_code_enabled":         true,
			"pre_save_enabled":           true,
			"logo":                       "/path/to/logo",
			"locked":                     false,
			"dolby_enabled":              false,
			"active":                     true,
			"live":                       false,
			"use_generic_package":        false,
			"stereo_required_for_dolby":  true,
			"sony_enabled":               false,
			"preferred_provider_program": true,
			"include_featured_artist":    false,
			"default":                    true,
			"featurefm_name":             "example_featurefm",
			"process_duration_days":      10,
			"ddex_version_id":            "123",
			"crs_permission_id":          456,
			"dolby_codec_id":             "7",
			"remote_path":                "/path/to/remote",
		}
		contextError := make(map[string]interface{})
		_, fieldsMap, err := suc.CreateStore(ctx, data, contextError, "stores", "post", map[string]models.ErrorResponse{})

		assert.NoError(t, err)
		assert.NotEmpty(t, fieldsMap)
	})

}

func TestUpdateStore(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}

	t.Run("InvalidInput", func(t *testing.T) {
		data := map[string]interface{}{
			"name":                       "Student store",
			"parent_id":                  "96b05633-69d7-4144-b0f9-73765b9ea7ca",
			"reporting_type":             "txt",
			"featurefm_enabled":          true,
			"price_code_enabled":         true,
			"pre_save_enabled":           true,
			"logo":                       "/path/to/logo",
			"locked":                     false,
			"dolby_enabled":              false,
			"active":                     true,
			"live":                       false,
			"use_generic_package":        false,
			"stereo_required_for_dolby":  true,
			"sony_enabled":               false,
			"preferred_provider_program": true,
			"include_featured_artist":    false,
			"default":                    true,
			"featurefm_name":             "example_featurefm",
			"process_duration_days":      10,
			"ddex_version_id":            "123",
			"crs_permission_id":          456,
			"dolby_codec_id":             "7",
			"remote_path":                "/path/to/remote",
		}
		contextError := make(map[string]interface{})

		storeID, err := uuid.Parse("4adc46e0-4d2d-467f-b31d-f772ea259725")
		if err != nil {
			t.Fatalf("Error parsing store ID: %v", err)
		}

		fieldsMap, err := suc.UpdateStore(ctx, storeID, data, contextError, "stores", "patch", map[string]models.ErrorResponse{})
		assert.Error(t, err)
		assert.NotEmpty(t, fieldsMap)
	})
}

func TestAddStoreRole(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}
	t.Run("InvalidInput", func(t *testing.T) {
		data := map[string]interface{}{
			"role_id":                 "invalid_role_id",
			"store_ingestion_spec_id": []interface{}{176, 177, 178},
		}
		_, fieldsMap, err := suc.AddStoreRole(ctx, "store_role", "POST", data, map[string]models.ErrorResponse{})
		assert.NoError(t, err)
		assert.NotEmpty(t, fieldsMap)
	})

}

func TestAddStoreGenre(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}
	t.Run("InvalidInput", func(t *testing.T) {
		data := map[string]interface{}{
			"genre_id":                "invalid_genre_id",
			"store_ingestion_spec_id": []interface{}{176, 177, 178},
		}
		_, fieldsMap, err := suc.AddStoreGenre(ctx, "store_genre", "POST", data, map[string]models.ErrorResponse{})
		assert.NoError(t, err)
		assert.NotEmpty(t, fieldsMap)
	})

}

func TestEditStoreGenre(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}
	t.Run("InvalidInput", func(t *testing.T) {
		data := map[string]interface{}{
			"store_genre": "test_genre_store",
		}
		fieldsMap, err := suc.EditStoreGenres(ctx, 1159, "store_genre", "PATCH", data, map[string]models.ErrorResponse{})
		assert.Error(t, err)
		assert.NotEmpty(t, fieldsMap)
	})

}

func TestEditStoreRole(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}
	t.Run("InvalidInput", func(t *testing.T) {
		data := map[string]interface{}{
			"store_role":              23,
			"is_user_defined":         "true",
			"is_resource_contributor": true,
		}
		fieldsMap, err := suc.EditStoreRoles(ctx, 1764, "store_role", "PATCH", data, map[string]models.ErrorResponse{})
		assert.Error(t, err)
		assert.NotEmpty(t, fieldsMap)
	})

}

func TestDeleteStoreRole(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()
	contextError := make(map[string]interface{})

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}
	t.Run("DeleteStoreRole", func(t *testing.T) {

		fieldsMap, err := suc.DeleteStoreRole(ctx, "76342485-a9b1-49ff-a3b7-2b628e814b38", contextError, "store_role", "DELETE", map[string]models.ErrorResponse{})
		assert.Error(t, err)
		assert.NotEmpty(t, fieldsMap)
	})

}

func TestDeleteStoreGenre(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()
	contextError := make(map[string]interface{})

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}
	t.Run("DeleteStoreGenre", func(t *testing.T) {
		fieldsMap, err := suc.DeleteStoreGenre(ctx, "76342485-a9b1-49ff-a3b7-2b628e814b38", contextError, "store_genre", "DELETE", map[string]models.ErrorResponse{})
		assert.Error(t, err)
		assert.NotEmpty(t, fieldsMap)
	})

}

func TestGetAllStoreIngestionIds(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()
	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}
	t.Run("GetAllStoreIngestionIds", func(t *testing.T) {
		fieldsMap, err := suc.GetAllStoreIngestionIds(ctx)
		assert.Error(t, err)
		assert.Empty(t, fieldsMap)
	})

	t.Run("GetActiveStoreIngestionIds", func(t *testing.T) {
		fieldsMap, err := suc.GetActiveStoreIngestionIds(ctx)
		assert.Error(t, err)
		assert.Empty(t, fieldsMap)
	})

	t.Run("GetStoreGenres", func(t *testing.T) {
		reqData := entities.ReqParams{Page: 1, Limit: 10}
		queryParam := []entities.KeyValuePair{{Key: "key1", Value: "value1"}}
		storeID := uuid.New()
		fieldsMap, _, err := suc.GetStoreGenres(ctx, reqData, queryParam, storeID)
		assert.Error(t, err)
		assert.Empty(t, fieldsMap)
	})

	t.Run("GetStoreRoles", func(t *testing.T) {
		reqData := entities.ReqParams{Page: 1, Limit: 10}
		queryParam := []entities.KeyValuePair{{Key: "key1", Value: "value1"}}
		storeID := uuid.New()
		fieldsMap, _, err := suc.GetStoreRoles(ctx, reqData, queryParam, storeID)
		assert.Error(t, err)
		assert.Empty(t, fieldsMap)
	})

}

func TestGetStores(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()
	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}
	params := entities.Params{
		Page:   "1",
		Limit:  "10",
		Sort:   "name",
		Order:  "asc",
		Status: "active",
	}

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	expectedStores := []entities.Store{
		{
			ID:   "02ead06c-c2f9-47d3-9c77-6de4546dba1b",
			Name: "127stoeaq1oiztoe",
		},
		{
			ID:   "06809e41-b8f5-41e8-920b-da33089cc3dd",
			Name: "Store20",
		},
	}

	mock.ExpectQuery(`SELECT (.+) FROM store`).
		WillReturnRows(sqlmock.NewRows([]string{"id", "name"}).
			AddRow(expectedStores[0].ID, expectedStores[0].Name).
			AddRow(expectedStores[1].ID, expectedStores[1].Name))

	t.Run("GetAllStores", func(t *testing.T) {
		fieldsMap, err := suc.GetStores(ctx, params)
		assert.Error(t, err)
		assert.Empty(t, fieldsMap)
	})

}

func TestStoreUseCases_CreateStore(t *testing.T) {
	ctx := context.Background()
	newStore := map[string]interface{}{
		"name":                  "test_store",
		"process_duration_days": 101,
	}
	contextError := make(map[string]interface{})
	endpoint := "/your-endpoint"
	method := "POST"
	errMap := make(map[string]models.ErrorResponse)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)

	expectedErrMap := map[string]models.ErrorResponse{
		"process_duration_days": {
			Code:    "",
			Message: []string{"invalid"},
		},
	}

	_, returnedErrMap, _ := suc.CreateStore(ctx, newStore, contextError, endpoint, method, errMap)
	assert.Equal(t, expectedErrMap, returnedErrMap)

}

func TestGetStoreByIDUsecase(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB, mock, _ := sqlmock.New()

	expectedStore := entities.Store{
		ID:                       "098b1e0c-5fc9-4ae0-a2d0-78abf283fec2",
		Name:                     "TestStore",
		RemotePath:               "TestPath",
		IsActive:                 false,
		IsLive:                   false,
		UseGenericPackage:        false,
		VariableName:             "variableName",
		ReportingType:            "xls",
		FeatureFMName:            "featureFMName",
		IsFeatureFMEnabled:       false,
		ProcessDurationDays:      0,
		DDEXVersionID:            "",
		CRSPermissionID:          0,
		IsDolbyEnabled:           false,
		DolbyCodecID:             "dolbyCodecID",
		IsStereoRequiredForDolby: false,
		IsSonyEnabled:            false,
		PreferredProviderProgram: false,
		IncludeFeaturedArtist:    false,
		IsPriceCodeEnabled:       false,
		IsPreSaveEnabled:         false,
		IsDefault:                false,
		Logo:                     "logo",
		IsLocked:                 false,
	}

	mock.ExpectQuery(`SELECT \* FROM store WHERE id = \$1`).
		WithArgs(expectedStore.ID).
		WillReturnError(sql.ErrNoRows)

	repoInstance := repo.NewStoreRepo(mockDB)
	ctx := context.Background()

	storeID := uuid.MustParse(expectedStore.ID)
	_, err := usecases.NewStoreUseCases(repoInstance, nil, nil).GetStoreByID(ctx, storeID)
	assert.Error(t, err)

}

func TestStoreUseCases_DeleteStore(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	storeID := uuid.New()

	ctx := context.Background()
	contextError := make(map[string]interface{})
	endpoint := "/stores"
	method := "DELETE"
	errMap := make(map[string]models.ErrorResponse)

	_, err = suc.DeleteStore(ctx, storeID, contextError, endpoint, method, errMap)
	assert.Error(t, err)

}

func TestGetPartnerDetailById(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}

}

func TestValidateStore(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database: %v", err)
	}
	defer db.Close()

	mockRepo := repo.NewStoreRepo(db)
	if mockRepo == nil {
		t.Fatal("Mock repository is nil")
	}

	suc := usecases.NewStoreUseCases(mockRepo, nil, nil)
	if suc == nil {
		t.Fatal("Store use case is nil")
	}

	t.Run("IsVariableNameExists", func(t *testing.T) {
		fieldsMap, err := mockRepo.IsVariableNameExists(ctx, "Lite plan1")
		assert.Error(t, err)
		assert.Empty(t, fieldsMap)
	})

	t.Run("GetReportingTypeValue", func(t *testing.T) {
		varName, err := mockRepo.GetReportingTypeValue(ctx, 234)
		assert.Error(t, err)
		assert.Empty(t, varName)
	})

	t.Run("IsRoleIdExists", func(t *testing.T) {
		err := mockRepo.AddGenresToStore(ctx, 158)
		assert.Error(t, err)
	})

	t.Run("GetAllStoreIngestionIds", func(t *testing.T) {
		varName, err := mockRepo.GetAllStoreIngestionIds(ctx)
		assert.Error(t, err)
		assert.Empty(t, varName)
	})

}
