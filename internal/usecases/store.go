package usecases

import (
	"context"
	"fmt"
	"math"
	"reflect"
	"store/internal/consts"
	"store/internal/entities"
	"store/internal/repo"
	"store/utilities"
	"strconv"
	"strings"

	cloud "gitlab.com/tuneverse/toolkit/core/cloud/awsutils"

	"github.com/google/uuid"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// StoreUseCases defines the use cases for store operations.
type StoreUseCases struct {
	repo  repo.StoreRepoImply
	cloud cloud.CloudServiceImply
	env   *entities.EnvConfig
}

// StoreUseCaseImply is an implementation of StoreUseCases.
type StoreUseCaseImply interface {
	CreateStore(ctx context.Context, newStore map[string]interface{}, contextError map[string]any, endpoint string,
		method string, errMap map[string]models.ErrorResponse) (string, map[string]models.ErrorResponse, error)
	UpdateStore(ctx context.Context, storeID uuid.UUID, updateData map[string]interface{}, contextError map[string]interface{}, endpoint string, method string, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error)
	GetStoreByID(ctx context.Context, id uuid.UUID) (*entities.Store, error)
	DeleteStore(ctx context.Context, storeID uuid.UUID, contextError map[string]any, endpoint string,
		method string, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error)
	GetStores(ctx context.Context, params entities.Params) (map[string]interface{}, error)
	GetStoreGenres(ctx context.Context, reqData entities.ReqParams, queryParam []entities.KeyValuePair, storeId uuid.UUID) ([]entities.StoreGenres, int, error)
	GetStoreRoles(ctx context.Context, reqData entities.ReqParams, queryParam []entities.KeyValuePair, storeId uuid.UUID) ([]entities.StoreRoles, int, error)
	EditStoreRoles(ctx context.Context, storeRoleId int64, endpoint string,
		method string, data map[string]interface{}, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error)
	EditStoreGenres(ctx context.Context, storeGenreId int64, endpoint string,
		method string, data map[string]interface{}, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error)
	// GetActiveStores(ctx context.Context) ([]entities.ActiveStoreDetail, error)
	AddStoreGenre(ctx context.Context, endpoint string,
		method string, data map[string]interface{}, errMap map[string]models.ErrorResponse) ([]int64, map[string]models.ErrorResponse, error)
	DeleteStoreGenre(ctx context.Context, storeGenreID string, contextError map[string]any, endpoint string,
		method string, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error)
	AddStoreRole(ctx context.Context, endpoint string,
		method string, data map[string]interface{}, errMap map[string]models.ErrorResponse) ([]int64, map[string]models.ErrorResponse, error)
	DeleteStoreRole(ctx context.Context, storeRoleID string, contextError map[string]any, endpoint string,
		method string, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error)
	GetAllStoreIngestionIds(ctx context.Context) ([]string, error)
	GetActiveStoreIngestionIds(ctx context.Context) ([]string, error)
	GetPartnerDetailById(ctx context.Context, partnerId string) (map[string]interface{}, error)
}

// NewStoreUseCases creates a new instance of StoreUseCases.
func NewStoreUseCases(storeRepo repo.StoreRepoImply, cloud cloud.CloudServiceImply, env *entities.EnvConfig) StoreUseCaseImply {
	return &StoreUseCases{
		repo:  storeRepo,
		cloud: cloud,
		env:   env,
	}
}

func (suc *StoreUseCases) AddStoreRole(ctx context.Context, endpoint string,
	method string, data map[string]interface{}, errMap map[string]models.ErrorResponse) ([]int64, map[string]models.ErrorResponse, error) {
	log := logger.Log().WithContext(ctx)
	if data != nil {
		if role_id, ok := data["role_id"].(string); ok {
			if len(strings.TrimSpace(role_id)) == 0 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "role_id", "required")
				errMap["role_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"required"},
				}
			} else {
				isExists, err := suc.repo.IsValidRoleId(ctx, role_id)
				if err != nil || !isExists {
					code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "role_id", "invalid")
					errMap["role_id"] = models.ErrorResponse{
						Code:    code,
						Message: []string{"invalid"},
					}
				}
			}

		} else {
			code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "role_id", "required")
			errMap["role_id"] = models.ErrorResponse{
				Code:    code,
				Message: []string{"required"},
			}
		}

		// Check and validate store_ingestion_spec_id
		if storeIngestionSpecIDs, ok := data["store_ingestion_spec_id"].([]interface{}); ok {
			validIDs := make([]int, 0)
			if len(storeIngestionSpecIDs) <= 0 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_ingestion_spec_id", "required")
				errMap["store_ingestion_spec_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"required"},
				}
			}
			for _, id := range storeIngestionSpecIDs {
				storeIngestionSpecID, ok := id.(float64) // Assuming the IDs come as float64
				if !ok || int(storeIngestionSpecID) <= 0 {
					code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_ingestion_spec_id", "invalid")
					errMap["store_ingestion_spec_id"] = models.ErrorResponse{
						Code:    code,
						Message: []string{"invalid"},
					}
				}
				storeIngestionSpecIDInt := int(storeIngestionSpecID)
				isExists, err := suc.repo.IsValidStoreIngestionSpecId(ctx, storeIngestionSpecIDInt)
				if err != nil || !isExists {
					code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_ingestion_spec_id", "invalid")
					errMap["store_ingestion_spec_id"] = models.ErrorResponse{
						Code:    code,
						Message: []string{"invalid"},
					}
				} else {
					validIDs = append(validIDs, storeIngestionSpecIDInt)
				}
			}
			if len(validIDs) > 0 {
				data["store_ingestion_spec_id"] = validIDs
			}
		} else {
			code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_ingestion_spec_id", "required")
			errMap["store_ingestion_spec_id"] = models.ErrorResponse{
				Code:    code,
				Message: []string{"required"},
			}
		}

	}
	if len(errMap) > 0 {
		return nil, errMap, nil
	} else {
		storeRoleIds, err := suc.repo.AddStoreRole(ctx, data)
		if err != nil {
			log.Errorf("AddStoreRole failed, err=%s", err.Error())
			return nil, errMap, err
		}
		return storeRoleIds, errMap, nil
	}

}

func (suc *StoreUseCases) AddStoreGenre(ctx context.Context, endpoint string,
	method string, data map[string]interface{}, errMap map[string]models.ErrorResponse) ([]int64, map[string]models.ErrorResponse, error) {
	log := logger.Log().WithContext(ctx)
	if data != nil {
		if genre_id, ok := data["genre_id"].(string); ok {
			if len(strings.TrimSpace(genre_id)) == 0 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "genre_id", "required")
				errMap["genre_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"required"},
				}
			} else {
				isExists, err := suc.repo.IsValidGenreId(ctx, genre_id)
				if err != nil || !isExists {
					code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "genre_id", "invalid")
					errMap["genre_id"] = models.ErrorResponse{
						Code:    code,
						Message: []string{"invalid"},
					}
				}
			}
		} else {
			code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "genre_id", "required")
			errMap["genre_id"] = models.ErrorResponse{
				Code:    code,
				Message: []string{"required"},
			}
		}

		if storeIngestionSpecIDs, ok := data["store_ingestion_spec_id"].([]interface{}); ok {
			validIDs := make([]int, 0)
			if len(storeIngestionSpecIDs) <= 0 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_ingestion_spec_id", "required")
				errMap["store_ingestion_spec_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"required"},
				}
			}
			for _, id := range storeIngestionSpecIDs {
				storeIngestionSpecID, ok := id.(float64) // Assuming the IDs come as float64
				if !ok || int(storeIngestionSpecID) <= 0 {
					code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_ingestion_spec_id", "invalid")
					errMap["store_ingestion_spec_id"] = models.ErrorResponse{
						Code:    code,
						Message: []string{"invalid"},
					}
				}
				storeIngestionSpecIDInt := int(storeIngestionSpecID)
				isExists, err := suc.repo.IsValidStoreIngestionSpecId(ctx, storeIngestionSpecIDInt)
				if err != nil || !isExists {
					code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_ingestion_spec_id", "invalid")
					errMap["store_ingestion_spec_id"] = models.ErrorResponse{
						Code:    code,
						Message: []string{"invalid"},
					}
				} else {
					validIDs = append(validIDs, storeIngestionSpecIDInt)
				}
			}
			data["store_ingestion_spec_id"] = validIDs
		} else {
			code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_ingestion_spec_id", "required")
			errMap["store_ingestion_spec_id"] = models.ErrorResponse{
				Code:    code,
				Message: []string{"required"},
			}
		}
	}
	storeGenreId, err := suc.repo.AddStoreGenre(ctx, data)
	if err != nil {
		log.Errorf("AddStoreGenres failed, err=%s", err.Error())
		return nil, errMap, err
	}
	return storeGenreId, errMap, nil
}

func (suc *StoreUseCases) EditStoreGenres(ctx context.Context, storeGenreId int64, endpoint string,
	method string, data map[string]interface{}, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error) {
	log := logger.Log().WithContext(ctx)
	isExists, err := suc.repo.IsStoreGenreExists(ctx, storeGenreId)
	if err != nil || !isExists {
		code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "common_messages", "notfound")
		errMap["common_messages"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"notfound"},
		}
	} else if data != nil {
		if store_genre_name, ok := data["store_genre"].(string); ok {
			if strings.TrimSpace(store_genre_name) == "" {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_genre", "required")
				errMap["store_genre"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"required"},
				}
			}
			if len(store_genre_name) < 3 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_genre", "min")
				errMap["store_genre"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"min"},
				}
			} else if len(store_genre_name) > 100 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_genre", "max")
				errMap["store_genre"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"max"},
				}
			}
			isDuplicateStoreGenre, _ := suc.repo.IsDuplicateStoreGenre(ctx, store_genre_name, storeGenreId)
			if isDuplicateStoreGenre {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_genre", "exists")
				errMap["store_genre"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"exists"},
				}
			}
			isInValid := utilities.ContainsSpecialChars(store_genre_name)
			if isInValid {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_genre", "invalid")
				errMap["store_genre"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
		}
	}

	err = suc.repo.EditStoreGenres(ctx, storeGenreId, data)
	if err != nil {
		log.Errorf("EditStoreGenres failed, err=%s", err.Error())
		return errMap, err
	}
	return errMap, nil
}

func (suc *StoreUseCases) EditStoreRoles(ctx context.Context, storeRoleId int64, endpoint string,
	method string, data map[string]interface{}, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error) {
	log := logger.Log().WithContext(ctx)
	isExists, err := suc.repo.IsStoreRoleExists(ctx, storeRoleId)
	if err != nil || !isExists {
		code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "common_messages", "notfound")
		errMap["common_messages"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"notfound"},
		}
	} else if data != nil {
		if store_role_name, ok := data["store_role"].(string); ok {
			if strings.TrimSpace(store_role_name) == "" {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_role", "required")
				errMap["store_role"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"required"},
				}
			}
			if len(store_role_name) < 3 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_role", "min")
				errMap["store_role"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"min"},
				}
			} else if len(store_role_name) > 100 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_role", "max")
				errMap["store_role"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"max"},
				}
			}
			isDuplicateStoreRole, _ := suc.repo.IsDuplicateStoreRole(ctx, store_role_name, storeRoleId)
			if isDuplicateStoreRole {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_role", "exists")
				errMap["store_role"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"exists"},
				}
			}
			isInValid := utilities.ContainsSpecialChars(store_role_name)
			if isInValid {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "store_role", "invalid")
				errMap["store_role"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
		}
	}
	err = suc.repo.EditStoreRoles(ctx, storeRoleId, data)
	if err != nil {
		log.Errorf("EditStoreRoles failed, err=%s", err.Error())
		return errMap, err
	}
	return errMap, nil
}

func (suc *StoreUseCases) GetStoreGenres(ctx context.Context, reqData entities.ReqParams, queryParam []entities.KeyValuePair, storeId uuid.UUID) ([]entities.StoreGenres, int, error) {
	offsetQuery := utilities.CalculateOffset(int(reqData.Page), int(reqData.Limit))
	storeGenres, totalCount, err := suc.repo.GetStoreGenres(ctx, offsetQuery, queryParam, reqData, storeId)
	if err != nil {
		return nil, 0, err
	}
	return storeGenres, totalCount, nil
}

func (suc *StoreUseCases) GetStoreRoles(ctx context.Context, reqData entities.ReqParams, queryParam []entities.KeyValuePair, storeId uuid.UUID) ([]entities.StoreRoles, int, error) {
	offsetQuery := utilities.CalculateOffset(int(reqData.Page), int(reqData.Limit))
	storeRoles, totalCount, err := suc.repo.GetStoreRoles(ctx, offsetQuery, queryParam, reqData, storeId)
	if err != nil {
		return nil, 0, err
	}
	return storeRoles, totalCount, nil
}

// CreateStore creates a new store use case.
func (suc *StoreUseCases) CreateStore(ctx context.Context, newStore map[string]interface{}, contextError map[string]any, endpoint string,
	method string, errMap map[string]models.ErrorResponse) (string, map[string]models.ErrorResponse, error) {
	if newStore != nil {
		if name, nameExists := newStore["name"].(string); !nameExists || strings.TrimSpace(name) == "" {
			code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", consts.Required)
			errMap["name"] = models.ErrorResponse{
				Code:    code,
				Message: []string{consts.Required},
			}
		} else {
			if len(name) < 1 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", consts.Min)
				errMap["name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Min},
				}
			} else if len(name) > 30 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", consts.Max)
				errMap["name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Max},
				}
			}
			isInValid := utilities.ContainsSpecialChars(name)
			if isInValid {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", consts.Invalid)
				errMap["name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Invalid},
				}
			}
			isDuplicateStore, _ := suc.repo.IsDuplicateStore(ctx, name)
			if isDuplicateStore {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", consts.Exists)
				errMap["name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Exists},
				}
			}
		}
		if parent_id, ok := newStore["parent_id"].(string); ok {
			isValidParent, _ := suc.repo.IsValidParentStore(ctx, strings.TrimSpace(parent_id))
			if len(parent_id) > 50 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "parent_id", consts.Max)
				errMap["parent_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Max},
				}
			} else if !isValidParent {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "parent_id", consts.Invalid)
				errMap["parent_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Invalid},
				}
			}
		}
		if ddexVersionId, ok := newStore["ddex_version_id"].(string); ok {
			validDdexVersionId, _ := suc.repo.IsValidDdexVersionId(ctx, ddexVersionId)
			if !validDdexVersionId {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "ddex_version_id", "invalid")
				errMap["ddex_version_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
		}
		if processDuration, ok := newStore["process_duration_days"].(float64); ok {
			// Successfully asserted to float64, now convert to int
			value := int(processDuration)
			if value > 100 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "process_duration_days", consts.Max)
				errMap["process_duration_days"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Max},
				}
			}
		} else {
			code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "process_duration_days", consts.Invalid)
			errMap["process_duration_days"] = models.ErrorResponse{
				Code:    code,
				Message: []string{consts.Invalid},
			}
		}
		if remote_path, ok := newStore["remote_path"].(string); ok {
			isInValid := utilities.ContainsSpecialChars(remote_path)
			isValidPath := utilities.IsValidPath(remote_path)
			if !isValidPath || isInValid {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "remote_path", consts.Invalid)
				errMap["remote_path"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Invalid},
				}
			}
			if len(remote_path) > 500 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "remote_path", consts.Max)
				errMap["remote_path"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Max},
				}
			}
		}
		if featurefm_name, ok := newStore["featurefm_name"].(string); ok {
			isInValid := utilities.ContainsSpecialChars(featurefm_name)
			if isInValid {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "featurefm_name", consts.Invalid)
				errMap["featurefm_name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Invalid},
				}
			}
			if len(featurefm_name) > 60 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "featurefm_name", consts.Max)
				errMap["featurefm_name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Max},
				}
			}
		}
		if dolby_codec_id, ok := newStore["dolby_codec_id"].(string); ok {
			isInValid := utilities.ContainsSpecialChars(dolby_codec_id)
			if isInValid {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "dolby_codec_id", consts.Invalid)
				errMap["dolby_codec_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Invalid},
				}
			}
			if len(dolby_codec_id) > 30 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "dolby_codec_id", consts.Max)
				errMap["dolby_codec_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Max},
				}
			}
		}

		if _, ok := newStore["reporting_type"].(string); ok {
			valueType := reflect.TypeOf(newStore["reporting_type"])
			if valueType.Kind() == reflect.String {
				if reportingType, ok := newStore["reporting_type"].(string); ok {
					isValid, err := suc.repo.IsValidReportingType(ctx, reportingType)
					if err != nil || !isValid {
						code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "reporting_type", consts.Invalid)
						errMap["reporting_type"] = models.ErrorResponse{
							Code:    code,
							Message: []string{consts.Invalid},
						}
					}
					if len(reportingType) > 30 {
						code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "reporting_type", consts.Max)
						errMap["reporting_type"] = models.ErrorResponse{
							Code:    code,
							Message: []string{consts.Max},
						}
					}
				}
			} else {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "reporting_type", consts.Invalid)
				errMap["reporting_type"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Invalid},
				}
			}
		}

		if logo, ok := newStore["logo"].(string); ok {
			isInValid := utilities.ContainsSpecialChars(logo)
			isValidPath := utilities.IsValidPath(logo)
			if isInValid || !isValidPath {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "logo", consts.Invalid)
				errMap["logo"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Invalid},
				}
			}
			if len(logo) >= 30 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "logo", consts.Max)
				errMap["logo"] = models.ErrorResponse{
					Code:    code,
					Message: []string{consts.Invalid},
				}
			}
		}

	}
	variableName := suc.repo.GenerateVariableName(ctx, newStore["name"].(string))
	if len(errMap) > 0 {
		return "", errMap, nil
	}
	storeID, err := suc.repo.CreateStore(ctx, newStore, variableName)
	return storeID, errMap, err

}

// UpdateStore updates a store use case.
func (suc *StoreUseCases) UpdateStore(ctx context.Context, storeID uuid.UUID, updateData map[string]interface{}, contextError map[string]interface{}, endpoint string, method string, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error) {
	if updateData != nil {
		if name, ok := updateData["name"].(string); ok {
			if strings.TrimSpace(name) == "" {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", "required")
				errMap["name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"required"},
				}
			}
			if len(name) < 1 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", "min")
				errMap["name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"min"},
				}
			} else if len(name) >= 30 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", "max")
				errMap["name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"max"},
				}
			}
			isDuplicateStore, _ := suc.repo.IsDuplicateStorePatch(ctx, name, storeID)
			if isDuplicateStore {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", "exists")
				errMap["name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"exists"},
				}
			}
			isInValid := utilities.ContainsSpecialChars(name)
			if isInValid {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "name", "invalid")
				errMap["name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
		}
		if parent_id, ok := updateData["parent_id"].(string); ok {
			isValidParent, _ := suc.repo.IsValidParentStore(ctx, strings.TrimSpace(parent_id))
			if len(parent_id) > 50 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "parent_id", "max")
				errMap["parent_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"max"},
				}
			}
			if !isValidParent {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "parent_id", "invalid")
				errMap["parent_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
		}
		if variable_name, ok := updateData["variable_name"].(string); ok {
			isValidParent, _ := suc.repo.IsVariableNameExists(ctx, variable_name)
			if !isValidParent {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "variable_name", "exists")
				errMap["variable_name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"exists"},
				}
			}
		}
		if processDurationRaw, ok := updateData["process_duration_days"]; ok {
			if processDuration, ok := processDurationRaw.(int); ok {
				// Successfully asserted to float64, now convert to int
				value := int(processDuration)
				if value > 100 {
					code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "process_duration_days", "max")
					errMap["process_duration_days"] = models.ErrorResponse{
						Code:    code,
						Message: []string{"max"},
					}
				} else if value < 0 {
					code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "process_duration_days", "invalid")
					errMap["process_duration_days"] = models.ErrorResponse{
						Code:    code,
						Message: []string{"invalid"},
					}
				}
			} else {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "process_duration_days", "invalid")
				errMap["process_duration_days"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
		}
		if remote_path, ok := updateData["remote_path"].(string); ok {
			isInValid := utilities.ContainsSpecialChars(remote_path)
			isValidPath := utilities.IsValidPath(remote_path)
			if isInValid || !isValidPath {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "remote_path", "invalid")
				errMap["remote_path"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
			if len(remote_path) > 500 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "remote_path", "max")
				errMap["remote_path"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"max"},
				}
			}
		}
		if featurefm_name, ok := updateData["featurefm_name"].(string); ok {
			isInValid := utilities.ContainsSpecialChars(featurefm_name)
			if isInValid {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "featurefm_name", "invalid")
				errMap["featurefm_name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
			if len(featurefm_name) > 60 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "featurefm_name", "max")
				errMap["featurefm_name"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"max"},
				}
			}
		}
		if dolby_codec_id, ok := updateData["dolby_codec_id"].(string); ok {
			isInValid := utilities.ContainsSpecialChars(dolby_codec_id)
			if isInValid {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "dolby_codec_id", "invalid")
				errMap["dolby_codec_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
			if len(dolby_codec_id) > 30 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "dolby_codec_id", "max")
				errMap["dolby_codec_id"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"max"},
				}
			}
		}
		if logo, ok := updateData["logo"].(string); ok {
			isInValid := utilities.ContainsSpecialChars(logo)
			isValidPath := utilities.IsValidPath(logo)
			if isInValid || !isValidPath {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "logo", "invalid")
				errMap["logo"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
			if len(logo) > 30 {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "logo", "max")
				errMap["logo"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"max"},
				}
			}
		}
		if reporting_type, ok := updateData["reporting_type"].(string); ok {
			valueType := reflect.TypeOf(reporting_type)
			if valueType.Kind() == reflect.String {
				if reportingType, ok := updateData["reporting_type"].(string); ok {
					isValid, _ := suc.repo.IsValidReportingType(ctx, reportingType)
					if !isValid {
						code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "reporting_type", "invalid")
						errMap["reporting_type"] = models.ErrorResponse{
							Code:    code,
							Message: []string{"invalid"},
						}
					}
					if len(reportingType) > 30 {
						code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "reporting_type", "max")
						errMap["reporting_type"] = models.ErrorResponse{
							Code:    code,
							Message: []string{"max"},
						}
					}
				}
			} else {
				code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "reporting_type", "invalid")
				errMap["reporting_type"] = models.ErrorResponse{
					Code:    code,
					Message: []string{"invalid"},
				}
			}
		}

	}
	err := suc.repo.UpdateStore(ctx, storeID, updateData)
	if err != nil {
		return errMap, err
	}

	return errMap, nil
}

// GetStoreByID retrieves a store by its ID.
func (suc *StoreUseCases) GetStoreByID(ctx context.Context, id uuid.UUID) (*entities.Store, error) {
	store, err := suc.repo.GetStoreByID(ctx, id)
	if err != nil {
		return nil, err
	}
	return store, nil
}

// func (suc *StoreUseCases) GetActiveStores(ctx context.Context) ([]entities.ActiveStoreDetail, error) {
// 	storeIds, err := suc.repo.GetActiveStores(ctx)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return storeIds, nil
// }

func (suc *StoreUseCases) GetAllStoreIngestionIds(ctx context.Context) ([]string, error) {
	storeIngestionIds, err := suc.repo.GetAllStoreIngestionIds(ctx)
	if err != nil {
		return nil, err
	}
	return storeIngestionIds, nil
}

func (suc *StoreUseCases) GetPartnerDetailById(ctx context.Context, partnerId string) (map[string]interface{}, error) {
	detail, err := suc.repo.GetPartnerDetailById(ctx, partnerId)
	if err != nil {
		return nil, err
	}
	return detail, nil
}

func (suc *StoreUseCases) GetActiveStoreIngestionIds(ctx context.Context) ([]string, error) {
	storeIngestionIds, err := suc.repo.GetActiveStoreIngestionIds(ctx)
	if err != nil {
		return nil, err
	}
	return storeIngestionIds, nil
}

// DeleteStore deletes a store use case.
func (suc *StoreUseCases) DeleteStore(ctx context.Context, storeID uuid.UUID, contextError map[string]any, endpoint string,
	method string, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error) {
	_, err := suc.repo.GetStoreByID(ctx, storeID)
	if err != nil {
		code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "id", "invalid")
		errMap["id"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid"},
		}
	}
	err = suc.repo.DeleteStore(ctx, storeID)
	if err != nil {
		return errMap, err
	}
	return errMap, nil
}

// DeleteStore deletes a store use case.
func (suc *StoreUseCases) DeleteStoreGenre(ctx context.Context, storeGenreID string, contextError map[string]any, endpoint string,
	method string, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error) {
	isExists, err := suc.repo.IsGenreIdExists(ctx, storeGenreID)
	if err != nil || !isExists {
		code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "genre_id", "invalid")
		errMap["genre_id"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid"},
		}
	}
	err = suc.repo.DeleteStoreGenre(ctx, storeGenreID)
	if err != nil {
		return errMap, err
	}
	return errMap, nil
}

// DeleteStore deletes a store use case.
func (suc *StoreUseCases) DeleteStoreRole(ctx context.Context, storeRoleID string, contextError map[string]any, endpoint string,
	method string, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error) {
	isExists, err := suc.repo.IsRoleIdExists(ctx, storeRoleID)
	if err != nil || !isExists {
		code, _ := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "role_id", "invalid")
		errMap["role_id"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid"},
		}
	}
	err = suc.repo.DeleteStoreRole(ctx, storeRoleID)
	if err != nil {
		return errMap, err
	}
	return errMap, nil
}

// GetStores retrieves all store use cases.
func (suc *StoreUseCases) GetStores(ctx context.Context, params entities.Params) (map[string]interface{}, error) {
	stores, totalCount, err := suc.repo.GetStores(ctx, params)
	if err != nil {
		return nil, err
	}
	pageInt, err := strconv.Atoi(params.Page)
	if err != nil {
		return nil, fmt.Errorf("failed to convert page to int: %w", err)
	}
	limitInt, err := strconv.Atoi(params.Limit)
	if err != nil {
		return nil, fmt.Errorf("failed to convert limit to int: %w", err)
	}

	totalPages := int32(math.Ceil(float64(totalCount) / float64(limitInt)))

	if params.ShowAll {
		limitInt = totalCount
	}

	metaData := entities.MetaData{
		Total:       int64(totalCount),
		PerPage:     int32(limitInt),
		CurrentPage: int32(pageInt),
		Next:        0,
		Prev:        0,
	}

	if metaData.CurrentPage < totalPages {
		metaData.Next = metaData.CurrentPage + 1
	}

	if metaData.CurrentPage > 1 {
		metaData.Prev = metaData.CurrentPage - 1
	}

	response := map[string]interface{}{
		"meta_data":   metaData,
		"stores":      stores,
		"total_count": metaData.Total,
	}

	return response, nil
}
