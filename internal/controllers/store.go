package controllers

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"reflect"
	"store/internal/consts"
	"store/internal/entities"
	"store/internal/usecases"
	"store/utilities"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/labstack/gommon/log"
	"gitlab.com/tuneverse/toolkit/core/activitylog"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/core/version"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/models/api"
	"gitlab.com/tuneverse/toolkit/utils"
)

// StoreController handles HTTP requests for store-related operations.
type StoreController struct {
	router      *gin.RouterGroup
	useCases    usecases.StoreUseCaseImply
	Cfg         *entities.EnvConfig
	activitylog *activitylog.ActivityLogOptions
}

// NewStoreController creates a new StoreController instance.
func NewStoreController(router *gin.RouterGroup, storeUseCase usecases.StoreUseCaseImply, cfg *entities.EnvConfig,
	activitylog *activitylog.ActivityLogOptions) *StoreController {
	return &StoreController{
		router:      router,
		useCases:    storeUseCase,
		Cfg:         cfg,
		activitylog: activitylog,
	}
}

// InitRoutes initializes routes for store-related operations.
func (store *StoreController) InitRoutes() {

	//health handler
	store.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "HealthHandler")
	})

	//store crud
	store.router.POST("/:version/stores", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "AddStore")
	})

	store.router.PATCH("/:version/stores/:store_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "UpdateStore")
	})

	store.router.DELETE("/:version/stores/:store_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "DeleteStore")
	})

	store.router.GET("/:version/stores/:store_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "GetSingleStore")
	})

	store.router.GET("/:version/stores", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "GetStores")
	})

	//Store roles
	store.router.GET("/:version/stores/:store_id/roles", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "GetStoreRoles")
	})

	store.router.PATCH("/:version/stores/roles/:store_role_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "EditStoreRoles")
	})

	store.router.POST("/:version/stores/roles", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "AddStoreRole")
	})

	store.router.DELETE("/:version/stores/roles/:role_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "DeleteStoreRole")
	})

	//Store genres
	store.router.GET("/:version/stores/:store_id/genres", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "GetStoreGenres")
	})

	store.router.PATCH("/:version/stores/genres/:store_genre_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "EditStoreGenres")
	})

	store.router.POST("/:version/stores/genres", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "AddStoreGenre")
	})

	store.router.DELETE("/:version/stores/genres/:genre_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "DeleteStoreGenre")
	})

	//other microservice endpoints
	store.router.GET("/:version/stores/store_ingestion_ids", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "GetAllStoreIngestionIds")
	})

	store.router.GET("/:version/stores/active_ingestion_ids", func(ctx *gin.Context) {
		version.RenderHandler(ctx, store, "GetActiveStoreIngestionIds")
	})

	// store.router.GET("/:version/stores/store_ids", func(ctx *gin.Context) {
	// 	version.RenderHandler(ctx, store, "GetActiveStores")
	// })

}

func (store *StoreController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}

func (store *StoreController) GetActiveStoreIngestionIds(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	helpLink := store.Cfg.ErrorHelpLink
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	storeIds, err := store.useCases.GetActiveStoreIngestionIds(ctx)
	if err != nil {
		log.Errorf("GetActiveStoreIngestionIds: Failed to GetStoreIds, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			log.Error("GetActiveStoreIngestionIds failed: error occured while retrieving errors from localization module %s", val)
		}
		log.Error("GetActiveStoreIngestionIds failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	var code int
	if storeIds == nil {
		code = http.StatusNoContent
	} else {
		code = http.StatusOK
	}

	resp := api.Response{
		Status:  "success",
		Message: "store ingestion ids retrieved successfully",
		Code:    code,
		Data:    storeIds,
		Errors:  struct{}{},
	}
	ctx.JSON(code, resp)
}

func (store *StoreController) GetAllStoreIngestionIds(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	helpLink := store.Cfg.ErrorHelpLink
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	storeIds, err := store.useCases.GetAllStoreIngestionIds(ctx)
	if err != nil {
		log.Errorf("GetStoreIngestionIds: Failed to GetStoreIds, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			log.Error("GetStoreIngestionIds failed: error occured while retrieving errors from localization module %s", val)
		}
		log.Error("GetStoreIngestionIds failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	var code int
	if storeIds == nil {
		code = http.StatusNoContent
	} else {
		code = http.StatusOK
	}

	resp := api.Response{
		Status:  "success",
		Message: "store ingestion ids retrieved successfully",
		Code:    code,
		Data:    storeIds,
		Errors:  struct{}{},
	}
	ctx.JSON(code, resp)
}

// func (store *StoreController) GetActiveStores(ctx *gin.Context) {
// 	ctxt := ctx.Request.Context()
// 	log := logger.Log().WithContext(ctxt)
// 	helpLink := store.Cfg.ErrorHelpLink
// 	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

// 	storeIds, err := store.useCases.GetActiveStores(ctx)
// 	if err != nil {
// 		log.Errorf("GetStoreIds: Failed to GetStoreIds, err=%s", err.Error())
// 		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
// 			"", contextError, "", "", nil, helpLink)
// 		if !hasError {
// 			log.Error("GetStoreIds failed: error occured while retrieving errors from localization module %s", val)
// 		}
// 		log.Error("GetStoreIds failed: invalid endpoint %s", val)
// 		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

// 		ctx.JSON(http.StatusBadRequest, result)
// 		return
// 	}

// 	var code int
// 	if storeIds == nil {
// 		code = http.StatusNoContent
// 	} else {
// 		code = http.StatusOK
// 	}

// 	resp := api.Response{
// 		Status:  "success",
// 		Message: "store ids retrieved successfully",
// 		Code:    code,
// 		Data:    storeIds,
// 		Errors:  struct{}{},
// 	}
// 	ctx.JSON(code, resp)
// }

func (store *StoreController) EditStoreRoles(ctx *gin.Context) {
	partnerID, _ := ctx.Get("partner_id")
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	storeRoleID := ctx.Param("store_role_id")
	serviceCode := make(map[string]string)
	errMap := utilities.NewErrorMap()
	helpLink := store.Cfg.ErrorHelpLink

	if len(storeRoleID) < 1 {
		log.Errorf("EditStoreRoles failed, store_role_id is missing")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "id is missing",
		})
		return
	}
	storeRoleIntValue, err := strconv.ParseInt(storeRoleID, 10, 64)
	if err != nil {
		fmt.Println("Error converting string to int64:", err)
		return
	}
	var store_role map[string]interface{}
	if err := ctx.Bind(&store_role); err != nil {
		log.Errorf("EditStoreRoles failed, Invalid json data, err=%s", err.Error())
		failureResp := api.Response{
			Status:  "failure",
			Message: "Invalid data",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	// Define ValidKeys struct to hold valid keys
	validKeys := entities.ValidStoreRolesKeys{}
	validKeysType := reflect.TypeOf(validKeys)

	// Check if the keys in the request body are valid
	invalidKeys := utilities.ValidateDataTypes(store_role, validKeysType, helpLink)

	if len(invalidKeys) > 0 {
		resp := api.Response{
			Status:  "failure",
			Message: "Validation error",
			Code:    400,
			Data:    gin.H{},
			Errors:  invalidKeys,
		}
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("Failed to update store role: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	fieldMap, err := store.useCases.EditStoreRoles(ctxt, storeRoleIntValue, endpoint, method, store_role, errMap)

	// service based codes are extracted here
	for key, value := range fieldMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			log.Error("EditStoreRoles failed: error occured while retrieving errors from localization module %s", val)
		}
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}

	if err != nil {
		log.Errorf("EditStoreRoles: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			log.Error("EditStoreRoles failed: error occured while retrieving errors from localization module %s", val)
		}
		log.Error("EditStoreRoles failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	resp := api.Response{
		Status:  "success",
		Message: "store role updated successfully",
		Code:    http.StatusOK,
		Data:    struct{}{},
		Errors:  struct{}{},
	}

	partnerDetail, err := store.useCases.GetPartnerDetailById(ctx, partnerID.(string))
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve partner details, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	data := map[string]interface{}{
		"base_url":        consts.StoreBaseURL,
		"partner_id":      partnerID,
		"partner_name":    partnerDetail["name"],
		"store_role_id":   storeRoleID,
		"store_role_name": consts.SampleStoreRoleName,
		"date_time":       time.Now(),
	}

	x, ok := data["partner_id"].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(x, "store_role_update", data)

		result, err := store.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error("Activity log failed: err=%s", err.Error(), result)

		}
	}

	ctx.JSON(http.StatusOK, resp)
}

func (store *StoreController) EditStoreGenres(ctx *gin.Context) {
	partnerID, _ := ctx.Get("partner_id")
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	storeGenreID := ctx.Param("store_genre_id")

	serviceCode := make(map[string]string)
	errMap := utilities.NewErrorMap()
	helpLink := store.Cfg.ErrorHelpLink

	if len(storeGenreID) < 1 {
		log.Errorf("EditStoreGenres failed, store_genre_id is missing")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "id is missing",
		})
		return
	}
	storeRoleIntValue, err := strconv.ParseInt(storeGenreID, 10, 64)
	if err != nil {
		fmt.Println("Error converting string to int64:", err)
		return
	}
	var store_genre map[string]interface{}
	if err := ctx.Bind(&store_genre); err != nil {
		log.Errorf("EditStoreGenres failed, Invalid json data, err=%s", err.Error())
		failureResp := api.Response{
			Status:  "failure",
			Message: "Invalid data",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	// Define ValidKeys struct to hold valid keys
	validKeys := entities.ValidStoreGenreKeys{}

	// Check if the keys in the request body are valid
	validKeysType := reflect.TypeOf(validKeys)
	invalidKeys := utilities.ValidateDataTypes(store_genre, validKeysType, helpLink)

	if len(invalidKeys) > 0 {
		resp := api.Response{
			Status:  "failure",
			Message: "Validation error",
			Code:    400,
			Data:    gin.H{},
			Errors:  invalidKeys,
		}
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("Failed to update store genre: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	fieldMap, err := store.useCases.EditStoreGenres(ctxt, storeRoleIntValue, endpoint, method, store_genre, errMap)

	// service based codes are extracted here
	for key, value := range fieldMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			log.Error("Failed to EditStoreGenres: error occured while retrieving errors from localization module %s", val)
		}
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}

	if err != nil {
		log.Errorf("Failed to EditStoreGenres: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			log.Error("Failed to EditStoreGenres: error occured while retrieving errors from localization module %s", val)
		}
		log.Error("Failed to EditStoreGenres: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusBadRequest, result)
		return

	}

	resp := api.Response{
		Status:  "success",
		Message: "store genre updated successfully",
		Code:    http.StatusOK,
		Data:    struct{}{},
		Errors:  struct{}{},
	}

	partnerDetail, err := store.useCases.GetPartnerDetailById(ctx, partnerID.(string))
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve partner details, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	data := map[string]interface{}{
		"base_url":         consts.StoreBaseURL,
		"partner_id":       partnerID,
		"partner_name":     partnerDetail["name"],
		"store_genre_id":   storeGenreID,
		"store_genre_name": consts.SampleStoreGenreName,
		"date_time":        time.Now(),
	}

	x, ok := data["partner_id"].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(x, "store_genre_updated", data)

		result, err := store.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error("Activity log failed: err=%s", err.Error(), result)

		}
	}

	ctx.JSON(http.StatusOK, resp)
}

func (store *StoreController) GetStoreRoles(ctx *gin.Context) {
	storeIDString := ctx.Param("store_id")
	if len(storeIDString) < 1 {
		log.Errorf("GetStoreRoles failed, store_id is missing")
		failureResp := api.Response{
			Status:  "failure",
			Message: "store id is missing",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	storeID, err := uuid.Parse(storeIDString)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("GetStoreRoles failed, Invalid Store Id error")
		failureResp := api.Response{
			Status:  "failure",
			Message: "store id not found",
			Code:    http.StatusNotFound,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusNotFound, failureResp)
		return
	}

	var reqParam entities.ReqParams
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	helpLink := store.Cfg.ErrorHelpLink

	if err := ctx.BindQuery(&reqParam); err != nil {
		log.Errorf("GetStoreRoles failed, Invalid json, err=%s", err.Error())
		failureResp := api.Response{
			Status:  "failure",
			Message: "Invalid data",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	serviceCode := make(map[string]string)
	errMap := utilities.NewErrorMap()

	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("Failed to update store genre: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	if reqParam.Page < 0 {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "page", "invalid_page")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("ListStoreRoles: Failed,error while loading service code")
		}
		errMap["page"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_page"},
		}
	}
	if reqParam.Limit < 0 {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "limit", "invalid_limit")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("ListStoreRoles: Failed,error while loading service code")
		}
		errMap["limit"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_limit"},
		}

	}

	if reqParam.Limit > consts.MaxLimit {
		invalidKeys := make(map[string][]entities.ErrorItem)
		invalidKeys["limit"] = append(invalidKeys["limit"], entities.ErrorItem{
			ErrorCode: "A1019",
			Field:     "limit",
			Help:      helpLink,
			Message:   []string{fmt.Sprintf("Limit exceeds the maximum allowed limit %d", consts.MaxLimit)},
		})
		failureResp := api.Response{
			Status:  "failure",
			Message: "Validation error",
			Code:    http.StatusTooManyRequests,
			Data:    struct{}{},
			Errors:  invalidKeys["limit"],
		}
		ctx.JSON(http.StatusTooManyRequests, failureResp)
		return
	}

	validSortFields := map[string]bool{"role": true, "store_role": true, "store_name": false}
	sort := reqParam.Sort
	order := strings.ToUpper(reqParam.Order)

	if sort != "" && !validSortFields[sort] {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "sort", "invalid_sort")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("ListStoreRoles: Failed,error while loading service code")
		}
		errMap["sort"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_sort"},
		}
	}
	if order != "" && (order != "ASC" && order != "DESC") {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "order", "invalid_order")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("ListStoreRoles: Failed,error while loading service code")
		}
		errMap["order"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_order"},
		}
	}

	for key, value := range errMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		for key, value := range errMap {
			serviceCode[key] = value.Code
		}
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			log.Error("GetStoreGenres failed: error occured while retrieving errors from localization module %s", val)
		}
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), val)

		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	validKeys := map[string]struct{}{
		"sort":       {},
		"order":      {},
		"page":       {},
		"limit":      {},
		"role":       {},
		"store_role": {},
	}

	var queryParams []entities.KeyValuePair
	for key, values := range ctx.Request.URL.Query() {
		// Allow only specific search fields (name, sku, partner)
		if key == "role" || key == "store_role" {
			for _, value := range values {
				queryParams = append(queryParams, entities.KeyValuePair{Key: key, Value: value})
			}
		} else if _, ok := validKeys[key]; !ok {
			failureResp := api.Response{
				Status:  "failure",
				Message: "Invalid search field '" + key + "'",
				Code:    http.StatusBadRequest,
				Data:    struct{}{},
				Errors:  struct{}{},
			}
			ctx.JSON(http.StatusBadRequest, failureResp)
			return
		}
	}

	isStoreFound, err := store.useCases.GetStoreByID(ctx, storeID)
	if err != nil || isStoreFound == nil {
		failureResp := api.Response{
			Status:  "failure",
			Message: "Store not found",
			Code:    http.StatusNotFound,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusNotFound, failureResp)
		return
	}

	storeRoles, totalCount, err := store.useCases.GetStoreRoles(ctxt, reqParam, queryParams, storeID)
	if err != nil {
		log.Errorf("GetStoreRoles: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			log.Error("GetStoreRoles failed: error occured while retrieving errors from localization module %s", val)
		}
		log.Error("GetStoreRoles failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusBadRequest, result)
		return

	}

	if reqParam.Limit == 0 {
		reqParam.Limit = 10
	}
	if reqParam.Page == 0 {
		reqParam.Page = 1
	}
	currentPage := int(reqParam.Page)
	nextPage := 0
	prevPage := 0
	totalPages := int(math.Ceil(float64(totalCount) / float64(reqParam.Limit)))
	if currentPage < totalPages {
		nextPage = currentPage + 1
	}

	if currentPage > 1 {
		prevPage = currentPage - 1
	}

	// Create and populate metadata
	metaData := entities.MetaData{
		Total:       int64(totalCount),
		PerPage:     int32(reqParam.Limit),
		CurrentPage: int32(currentPage),
		Next:        int32(nextPage),
		Prev:        int32(prevPage),
	}

	var code int
	if storeRoles == nil {
		code = http.StatusNoContent
	} else {
		code = http.StatusOK
	}

	data := entities.ResponseMetaData{
		Data:     storeRoles,
		MetaData: utilities.MetaDataInfo(metaData),
	}
	resp := api.Response{
		Status:  "success",
		Message: "store roles retrieved successfully",
		Code:    code,
		Data:    data,
		Errors:  struct{}{},
	}
	ctx.JSON(code, resp)
}

func (store *StoreController) GetStoreGenres(ctx *gin.Context) {
	storeIDString := ctx.Param("store_id")
	if len(storeIDString) < 1 {
		log.Errorf("GetStoreGenres failed, store_id is missing")
		failureResp := api.Response{
			Status:  "failure",
			Message: "store id is missing",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	storeID, err := uuid.Parse(storeIDString)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("GetStoreGenres failed, Invalid Store Id error")
		failureResp := api.Response{
			Status:  "failure",
			Message: "store id not found",
			Code:    http.StatusNotFound,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusNotFound, failureResp)
		return
	}
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	var reqParam entities.ReqParams
	helpLink := store.Cfg.ErrorHelpLink

	if err := ctx.BindQuery(&reqParam); err != nil {
		log.Errorf("GetStoreGenres failed, Invalid json, err=%s", err.Error())
		failureResp := api.Response{
			Status:  "failure",
			Message: "Invalid data",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	serviceCode := make(map[string]string)
	errMap := utilities.NewErrorMap()

	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("Failed to update store genre: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	if reqParam.Page < 0 {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "page", "invalid_page")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetStoreGenres: Failed,error while loading service code")
		}
		errMap["page"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_page"},
		}
	}
	if reqParam.Limit < 0 {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "limit", "invalid_limit")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetStoreGenres: Failed,error while loading service code")
		}
		errMap["limit"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_limit"},
		}

	}

	if reqParam.Limit > consts.MaxLimit {
		invalidKeys := make(map[string][]entities.ErrorItem)
		invalidKeys["limit"] = append(invalidKeys["limit"], entities.ErrorItem{
			ErrorCode: "A1019",
			Field:     "limit",
			Help:      helpLink,
			Message:   []string{fmt.Sprintf("Limit exceeds the maximum allowed limit %d", consts.MaxLimit)},
		})
		failureResp := api.Response{
			Status:  "failure",
			Message: "Validation error",
			Code:    http.StatusTooManyRequests,
			Data:    struct{}{},
			Errors:  invalidKeys["limit"],
		}
		ctx.JSON(http.StatusTooManyRequests, failureResp)
		return
	}

	validSortFields := map[string]bool{"genre": true, "store_genre": true, "store_name": false}
	sort := reqParam.Sort
	order := strings.ToUpper(reqParam.Order)

	if sort != "" && !validSortFields[sort] {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "sort", "invalid_sort")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetStoreGenres: Failed,error while loading service code")
		}
		errMap["sort"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_sort"},
		}
	}
	if order != "" && (order != "ASC" && order != "DESC") {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "order", "invalid_order")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetStoreGenres: Failed,error while loading service code")
		}
		errMap["order"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_order"},
		}
	}

	for key, value := range errMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		for key, value := range errMap {
			serviceCode[key] = value.Code
		}
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			log.Error("GetStoreGenres failed: error occured while retrieving errors from localization module %s", val)
		}
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), val)

		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	validKeys := map[string]struct{}{
		"sort":        {},
		"order":       {},
		"page":        {},
		"limit":       {},
		"genre":       {},
		"store_genre": {},
	}

	var queryParams []entities.KeyValuePair
	for key, values := range ctx.Request.URL.Query() {
		// Allow only specific search fields (name, sku, partner)
		if key == "genre" || key == "store_genre" {
			for _, value := range values {
				queryParams = append(queryParams, entities.KeyValuePair{Key: key, Value: value})
			}
		} else if _, ok := validKeys[key]; !ok {
			failureResp := api.Response{
				Status:  "failure",
				Message: "Invalid search field '" + key + "'",
				Code:    http.StatusBadRequest,
				Data:    struct{}{},
				Errors:  struct{}{},
			}
			ctx.JSON(http.StatusBadRequest, failureResp)
			return
		}
	}

	isStoreFound, err := store.useCases.GetStoreByID(ctx, storeID)
	if err != nil || isStoreFound == nil {
		failureResp := api.Response{
			Status:  "failure",
			Message: "Store not found",
			Code:    http.StatusNotFound,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusNotFound, failureResp)
		return
	}

	storeGenres, totalCount, err := store.useCases.GetStoreGenres(ctxt, reqParam, queryParams, storeID)
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetStoreGenres: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Errorf("GetStoreGenres failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Errorf("GetStoreGenres failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	if reqParam.Limit == 0 {
		reqParam.Limit = 10
	}
	if reqParam.Page == 0 {
		reqParam.Page = 1
	}
	currentPage := int(reqParam.Page)
	nextPage := 0
	prevPage := 0
	totalPages := int(math.Ceil(float64(totalCount) / float64(reqParam.Limit)))
	if currentPage < totalPages {
		nextPage = currentPage + 1
	}

	if currentPage > 1 {
		prevPage = currentPage - 1
	}

	// Create and populate metadata
	metaData := entities.MetaData{
		Total:       int64(totalCount),
		PerPage:     int32(reqParam.Limit),
		CurrentPage: int32(currentPage),
		Next:        int32(nextPage),
		Prev:        int32(prevPage),
	}

	data := entities.ResponseMetaData{
		Data:     storeGenres,
		MetaData: utilities.MetaDataInfo(metaData),
	}

	var code int

	if storeGenres == nil {
		code = http.StatusNoContent
	} else {
		code = http.StatusOK
	}

	resp := api.Response{
		Status:  "success",
		Message: "store genres retrieved successfully",
		Code:    code,
		Data:    data,
		Errors:  struct{}{},
	}
	ctx.JSON(code, resp)
}

// AddStore handles the HTTP request to add a store.
func (store *StoreController) AddStore(ctx *gin.Context) {
	partnerID, _ := ctx.Get("partner_id")
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	ctxt := ctx.Request.Context()
	errMap := utilities.NewErrorMap()
	serviceCode := make(map[string]string)
	helpLink := store.Cfg.ErrorHelpLink

	// Parse the request body into a Store struct
	var isValidData entities.Store
	isValidData.IsActive = true
	isValidData.IsLive = false

	if err := ctx.ShouldBindJSON(&isValidData); err != nil {
		logger.Log().WithContext(ctxt).Errorf("Failed to add store: invalid input")
		fmt.Printf("Error: %T - %s\n", err, err.Error())
		fieldErrors := make(map[string][]entities.ErrorItem)

		// Handle different types of errors
		switch err := err.(type) {
		case *json.UnmarshalTypeError:
			expectedType := err.Type.String()
			if expectedType == "bool" {
				expectedType = "boolean"
			}
			customErrorMessage := fmt.Sprintf("Field '%s' must be %s value.", err.Field, expectedType)

			// Append error to the corresponding field in the map
			fieldErrors[err.Field] = append(fieldErrors[err.Field], entities.ErrorItem{
				ErrorCode: "A1019",
				Field:     err.Field,
				Help:      helpLink,
				Message:   []string{customErrorMessage},
			})

		default:
			// Handle other types of errors here
			ctx.JSON(http.StatusUnprocessableEntity, api.Response{
				Status:  "failure",
				Message: "Invalid request body.",
				Code:    http.StatusUnprocessableEntity,
				Data:    struct{}{},
				Errors:  struct{}{},
			})
			return
		}

		// Construct the error response
		failureResp := api.Response{
			Status:  "failure",
			Message: "Validation error",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  fieldErrors,
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	// Convert isValidData to map[string]interface{}
	jsonData, err := json.Marshal(isValidData)
	if err != nil {
		failureResp := api.Response{
			Status:  "failure",
			Message: "Internal server error",
			Code:    http.StatusInternalServerError,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusInternalServerError, failureResp)
		return
	}

	var newData map[string]interface{}
	if err := json.Unmarshal(jsonData, &newData); err != nil {
		failureResp := api.Response{
			Status:  "failure",
			Message: "Internal server error",
			Code:    http.StatusInternalServerError,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusInternalServerError, failureResp)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("AddStore failed: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	storeID, fieldMap, err := store.useCases.CreateStore(ctxt, newData, contextError, endpoint, method, errMap)
	for key, value := range fieldMap {
		serviceCode[key] = value.Code
	}
	if len(errMap) != 0 {
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			logger.Log().WithContext(ctxt).Errorf("AddStore failed: error occured while retrieving errors from localization module %s", val)
		}
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("AddStore: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Errorf("AddStore failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Errorf("AddStore failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusBadRequest, result)
		return

	}

	logger.Log().WithContext(ctxt).Errorf("AddStore: completed")

	resp := api.Response{
		Status:  "success",
		Message: consts.Success,
		Code:    http.StatusCreated,
		Data:    struct{}{},
		Errors:  struct{}{},
	}

	partnerDetail, _ := store.useCases.GetPartnerDetailById(ctx, partnerID.(string))

	data := map[string]interface{}{
		"base_url":     consts.StoreBaseURL,
		"partner_id":   partnerID,
		"partner_name": partnerDetail["name"],
		"store_id":     storeID,
		"store_name":   isValidData.Name,
		"date_time":    time.Now(),
	}

	x, ok := data["partner_id"].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(x, "store_added", data)

		result, err := store.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error("Activity log failed: err=%s", err.Error(), result)

		}
	}

	ctx.JSON(http.StatusCreated, resp)
}

// AddStoreRole handles the HTTP request to add a store.
func (store *StoreController) AddStoreRole(ctx *gin.Context) {
	partnerID, _ := ctx.Get("partner_id")
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)

	serviceCode := make(map[string]string)
	errMap := utilities.NewErrorMap()
	helpLink := store.Cfg.ErrorHelpLink

	var store_role map[string]interface{}
	if err := ctx.Bind(&store_role); err != nil {
		log.Errorf("AddStoreRole failed, Invalid json data, err=%s", err.Error())
		failureResp := api.Response{
			Status:  "failure",
			Message: "Invalid data",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("Failed to add store role: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	storeRoleIds, fieldMap, err := store.useCases.AddStoreRole(ctxt, endpoint, method, store_role, errMap)

	// service based codes are extracted here
	for key, value := range fieldMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			log.Error("Failed to AddStoreRole: error occured while retrieving errors from localization module %s", val)
		}
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}

	if err != nil {
		log.Errorf("Failed to AddStoreRole: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			log.Error("Failed to AddStoreRole: error occured while retrieving errors from localization module %s", val)
		}
		log.Error("Failed to AddStoreRole: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	resp := api.Response{
		Status:  "success",
		Message: "store role added successfully",
		Code:    http.StatusOK,
		Data:    struct{}{},
		Errors:  struct{}{},
	}

	partnerDetail, err := store.useCases.GetPartnerDetailById(ctx, partnerID.(string))
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve partner details, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	storeRoleIdString, _ := utilities.Int64ToStringConvert(storeRoleIds)

	data := map[string]interface{}{
		"base_url":        consts.StoreBaseURL,
		"partner_id":      partnerID,
		"partner_name":    partnerDetail["name"],
		"store_role_id":   storeRoleIdString,
		"store_role_name": consts.SampleStoreRoleName,
		"date_time":       time.Now(),
	}

	x, ok := data["partner_id"].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(x, "store_role_add", data)

		result, err := store.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error("Activity log failed: err=%s", err.Error(), result)

		}
	}
	ctx.JSON(http.StatusOK, resp)
}

// AddStoreGenre handles the HTTP request to add a store.
func (store *StoreController) AddStoreGenre(ctx *gin.Context) {
	partnerID, _ := ctx.Get("partner_id")
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)

	serviceCode := make(map[string]string)
	errMap := utilities.NewErrorMap()
	helpLink := store.Cfg.ErrorHelpLink

	var store_genre map[string]interface{}
	if err := ctx.Bind(&store_genre); err != nil {
		log.Errorf("AddStoreGenre failed, Invalid json data, err=%s", err.Error())
		failureResp := api.Response{
			Status:  "failure",
			Message: "Invalid data",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("Failed to add store genre: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	storegenreIds, fieldMap, err := store.useCases.AddStoreGenre(ctxt, endpoint, method, store_genre, errMap)

	// service based codes are extracted here
	for key, value := range fieldMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			log.Error("Failed to AddStoreGenre: error occured while retrieving errors from localization module %s", val)
		}
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}

	if err != nil {
		log.Errorf("Failed to AddStoreGenre: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			log.Error("Failed to AddStoreGenre: error occured while retrieving errors from localization module %s", val)
		}
		log.Error("Failed to AddStoreGenre: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusBadRequest, result)
		return

	}

	resp := api.Response{
		Status:  "success",
		Message: "store genre added successfully",
		Code:    http.StatusOK,
		Data:    struct{}{},
		Errors:  struct{}{},
	}

	partnerDetail, err := store.useCases.GetPartnerDetailById(ctx, partnerID.(string))
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve partner details, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	storeGenreeIdString, _ := utilities.Int64ToStringConvert(storegenreIds)

	data := map[string]interface{}{
		"base_url":         consts.StoreBaseURL,
		"partner_id":       partnerID,
		"partner_name":     partnerDetail["name"],
		"store_genre_id":   storeGenreeIdString,
		"store_genre_name": consts.SampleStoreGenreName,
		"date_time":        time.Now(),
	}

	x, ok := data["partner_id"].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(x, "store_genre_added", data)

		result, err := store.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error("Activity log failed: err=%s", err.Error(), result)

		}
	}

	ctx.JSON(http.StatusOK, resp)
}

func (store *StoreController) DeleteStoreRole(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	storeRoleID := ctx.Param("role_id")
	partnerID, _ := ctx.Get("partner_id")
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	errMap := utilities.NewErrorMap()
	serviceCode := make(map[string]string)
	helpLink := store.Cfg.ErrorHelpLink
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("failed to delete store role: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	fieldMap, err := store.useCases.DeleteStoreRole(ctx, storeRoleID, contextError, endpoint, method, errMap)
	for key, value := range fieldMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			logger.Log().WithContext(ctxt).Errorf("DeleteStoreRole failed: error occured while retrieving errors from localization module %s", val)
		}
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("DeleteStoreRole: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Errorf("DeleteStoreRole failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Errorf("DeleteStoreRole failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	resp := api.Response{
		Status:  "success",
		Message: "Store role deleted successfully",
		Code:    http.StatusOK,
		Data:    struct{}{},
		Errors:  struct{}{},
	}

	partnerDetail, err := store.useCases.GetPartnerDetailById(ctx, partnerID.(string))
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve partner details, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	data := map[string]interface{}{
		"base_url":        consts.StoreBaseURL,
		"partner_id":      partnerID,
		"partner_name":    partnerDetail["name"],
		"store_role_id":   storeRoleID,
		"store_role_name": consts.SampleStoreRoleName,
		"date_time":       time.Now(),
	}

	x, ok := data["partner_id"].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(x, "store_role_delete", data)

		result, err := store.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error("Activity log failed: err=%s", err.Error(), result)

		}
	}
	ctx.JSON(http.StatusOK, resp)
}

func (store *StoreController) DeleteStoreGenre(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	storeGenreID := ctx.Param("genre_id")
	partnerID, _ := ctx.Get("partner_id")
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	errMap := utilities.NewErrorMap()
	serviceCode := make(map[string]string)
	helpLink := store.Cfg.ErrorHelpLink
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("failed to delete store genre: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	fieldMap, err := store.useCases.DeleteStoreGenre(ctx, storeGenreID, contextError, endpoint, method, errMap)
	for key, value := range fieldMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			logger.Log().WithContext(ctxt).Errorf("DeleteStoreGenre failed: error occured while retrieving errors from localization module %s", val)
		}
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("DeleteStoreGenre: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Errorf("DeleteStoreGenre failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Errorf("DeleteStoreGenre failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	resp := api.Response{
		Status:  "success",
		Message: "Store genre deleted successfully",
		Code:    http.StatusOK,
		Data:    struct{}{},
		Errors:  struct{}{},
	}

	partnerDetail, err := store.useCases.GetPartnerDetailById(ctx, partnerID.(string))
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve partner details, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	data := map[string]interface{}{
		"base_url":         consts.StoreBaseURL,
		"partner_id":       partnerID,
		"partner_name":     partnerDetail["name"],
		"store_genre_id":   storeGenreID,
		"store_genre_name": consts.SampleStoreGenreName,
		"date_time":        time.Now(),
	}

	x, ok := data["partner_id"].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(x, "store_genre_deleted", data)

		result, err := store.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error("Activity log failed: err=%s", err.Error(), result)

		}
	}

	ctx.JSON(http.StatusOK, resp)
}

// For success response
func SuccessResponseGenerator(message string, code int, data any) api.Response {
	var result api.Response
	if data == "" {
		data = map[string]interface{}{}
	}
	result = api.Response{Status: consts.SuccessKey, Message: message, Code: code, Data: data, Errors: map[string]string{}}
	return result
}

// For error response
func ErrorResponseGenerator(message string, code int, errors any) api.Response {
	var result api.Response
	if errors == "" {
		errors = map[string]interface{}{}
	}
	result = api.Response{Status: consts.Failure, Message: message, Code: code, Data: map[string]string{}, Errors: errors}
	return result
}

// UpdateStore handles the HTTP request to update a store.
func (store *StoreController) UpdateStore(ctx *gin.Context) {
	partnerID, _ := ctx.Get("partner_id")
	ctxt := ctx.Request.Context()
	storeIDStr := ctx.Param("store_id")
	errMap := utilities.NewErrorMap()
	serviceCode := make(map[string]string)
	helpLink := store.Cfg.ErrorHelpLink

	contextError, ok := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	if !ok {
		failureResp := api.Response{
			Status:  "failure",
			Message: "Internal Server Error",
			Code:    http.StatusInternalServerError,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusInternalServerError, failureResp)
		return
	}

	storeID, err := uuid.Parse(storeIDStr)
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("UpdateStore failed, Invalid Store Id error")
		failureResp := api.Response{
			Status:  "failure",
			Message: "Invalid store ID",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	// Parse the request body into a Store struct
	var updateData map[string]interface{}
	if err := ctx.BindJSON(&updateData); err != nil {
		if jsonError, ok := err.(*json.UnmarshalTypeError); ok {
			expectedType := jsonError.Type.String()
			if expectedType == "bool" {
				expectedType = "boolean"
			}
			customErrorMessage := fmt.Sprintf("Field '%s' must be %s value.", jsonError.Field, expectedType)
			ctx.Header("Content-Type", "application/json")
			failureResp := api.Response{
				Status:  "failure",
				Message: customErrorMessage,
				Code:    http.StatusBadRequest,
				Data:    struct{}{},
				Errors:  struct{}{},
			}
			ctx.JSON(http.StatusBadRequest, failureResp)
		}
		return
	}
	if crsPermissionID, ok := updateData["crs_permission_id"].(float64); ok {
		updateData["crs_permission_id"] = int(crsPermissionID)
	}
	if processDurationDays, ok := updateData["process_duration_days"].(float64); ok {
		updateData["process_duration_days"] = int(processDurationDays)
	}

	// Check if request body is empty
	if len(updateData) == 0 {
		failureResp := api.Response{
			Status:  "failure",
			Message: "Request body is empty",
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	// Check if the keys in the request body are valid
	validKeys := entities.Store{}
	validKeysType := reflect.TypeOf(validKeys)
	invalidKeys := utilities.ValidateDataTypes(updateData, validKeysType, helpLink)

	// Check if there are any validation errors
	if len(invalidKeys) > 0 {
		resp := api.Response{
			Status:  "Failure",
			Message: "Validation error",
			Code:    400,
			Data:    gin.H{},
			Errors:  invalidKeys,
		}
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		logger.Log().WithContext(ctxt).Errorf("Add Partner Specific Permissions failed: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	fieldMap, err := store.useCases.UpdateStore(ctxt, storeID, updateData, contextError, endpoint, method, errMap)
	for key, value := range fieldMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			logger.Log().WithContext(ctxt).Errorf("Failed to update store: validation error")
		}
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("ListProductLog: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("ListProductLog failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	resp := api.Response{
		Status:  "success",
		Message: "Store updated successfully",
		Code:    http.StatusOK,
		Data:    struct{}{},
		Errors:  struct{}{},
	}

	partnerDetail, err := store.useCases.GetPartnerDetailById(ctx, partnerID.(string))
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve partner details, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	storeDetail, err := store.useCases.GetStoreByID(ctx, storeID)
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve store, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	data := map[string]interface{}{
		"base_url":     consts.StoreBaseURL,
		"partner_id":   partnerID,
		"partner_name": partnerDetail["name"],
		"store_id":     storeIDStr,
		"store_name":   storeDetail.Name,
		"date_time":    time.Now(),
	}

	x, ok := data["partner_id"].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(x, "store_updated", data)

		result, err := store.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error("Activity log failed: err=%s", err.Error(), result)
		}
	}

	ctx.JSON(http.StatusOK, resp)
}

// DeleteStore handles the HTTP request to delete a store.
func (store *StoreController) DeleteStore(ctx *gin.Context) {
	partnerID, _ := ctx.Get("partner_id")
	ctxt := ctx.Request.Context()
	storeIDStr := ctx.Param("store_id")
	storeID, _ := uuid.Parse(storeIDStr)
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	errMap := utilities.NewErrorMap()
	serviceCode := make(map[string]string)
	helpLink := store.Cfg.ErrorHelpLink
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("Add Partner Specific Permissions failed: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	fieldMap, err := store.useCases.DeleteStore(ctx, storeID, contextError, endpoint, method, errMap)
	for key, value := range fieldMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			logger.Log().WithContext(ctxt).Errorf("DeleteStore failed: error occured while retrieving errors from localization module %s", val)
		}
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("ListProductLog: Failed to retrieve logs, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Errorf("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Errorf("ListProductLog failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	resp := api.Response{
		Status:  "success",
		Message: "Store deleted successfully",
		Code:    http.StatusOK,
		Data:    struct{}{},
		Errors:  struct{}{},
	}

	partnerDetail, err := store.useCases.GetPartnerDetailById(ctx, partnerID.(string))
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve partner details, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	storeDetail, err := store.useCases.GetStoreByID(ctx, storeID)
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetPartnerDetailById: Failed to retrieve store, err=%s", err.Error())
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Error("ActivityLog failed: error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Error("GetPartnerDetailById failed: invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	data := map[string]interface{}{
		"base_url":     consts.StoreBaseURL,
		"partner_id":   partnerID,
		"partner_name": partnerDetail["name"],
		"store_id":     storeID,
		"store_name":   storeDetail.Name,
		"date_time":    time.Now(),
	}

	x, ok := data["partner_id"].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(x, "store_deleted", data)

		result, err := store.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error("Activity log failed: err=%s", err.Error(), result)
		}
	}

	ctx.JSON(http.StatusOK, resp)
}

// GetStores handles the HTTP request to get all stores.
func (store *StoreController) GetStores(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	helpLink := store.Cfg.ErrorHelpLink

	validKeys := map[string]bool{
		"name":      true,
		"sort":      true,
		"order":     true,
		"status":    true,
		"live":      true,
		"page":      true,
		"limit":     true,
		"store_ids": true,
	}

	// Check if the keys in the query parameters are valid
	invalidKeys := []string{}
	for key := range ctx.Request.URL.Query() {
		if _, ok := validKeys[key]; !ok {
			invalidKeys = append(invalidKeys, key)
		}
	}

	// If invalid keys are found, return an error response
	if len(invalidKeys) > 0 {
		failureResp := api.Response{
			Status:  "failure",
			Message: "Invalid query parameter key(s): " + strings.Join(invalidKeys, ", "),
			Code:    http.StatusBadRequest,
			Data:    struct{}{},
			Errors:  struct{}{},
		}
		ctx.JSON(http.StatusBadRequest, failureResp)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	serviceCode := make(map[string]string)
	errMap := utilities.NewErrorMap()

	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf("Failed to get stores: invalid endpoint %s", val)
		result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	pageVal := ctx.Query("page")
	page := 1
	if pageVal != "" {
		var err error
		page, err = strconv.Atoi(pageVal)
		if err != nil {
			val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
				"", contextError, "", "", nil, helpLink)
			if !hasError {
				logger.Log().WithContext(ctxt).Error("GetStore failed: error occured while retrieving errors from localization module %s", val)
			}
			result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: map[string]string{}}
			ctx.JSON(http.StatusBadRequest, result)
			return
		}
	}

	perPageVal := ctx.Query("limit")
	perPage := consts.ShowAllLimit
	if perPageVal != "" {
		var err error
		perPage, err = strconv.Atoi(perPageVal)
		if err != nil {
			val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
				"", contextError, "", "", nil, helpLink)
			if !hasError {
				logger.Log().WithContext(ctxt).Error("GetStore failed: error occured while retrieving errors from localization module %s", val)
			}
			result := api.Response{Status: consts.Failure, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: map[string]string{}}
			ctx.JSON(http.StatusBadRequest, result)
			return
		}
	}

	pageStr := strconv.Itoa(page)
	perPageStr := strconv.Itoa(perPage)

	if page <= 0 {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "page", "invalid_page")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("ListStoreRoles: Failed,error while loading service code")
		}
		errMap["page"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_page"},
		}
	}

	if perPage <= 0 {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "limit", "invalid_limit")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("ListStoreRoles: Failed,error while loading service code")
		}
		errMap["limit"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_limit"},
		}

	}

	if perPage > consts.MaxLimit && perPageVal != "" {
		invalidKeys := make(map[string][]entities.ErrorItem)
		invalidKeys["limit"] = append(invalidKeys["limit"], entities.ErrorItem{
			ErrorCode: "A1019",
			Field:     "limit",
			Help:      helpLink,
			Message:   []string{fmt.Sprintf("Limit exceeds the maximum allowed limit %d", consts.MaxLimit)},
		})
		failureResp := api.Response{
			Status:  "failure",
			Message: "Validation error",
			Code:    http.StatusTooManyRequests,
			Data:    struct{}{},
			Errors:  invalidKeys["limit"],
		}
		ctx.JSON(http.StatusTooManyRequests, failureResp)
		return
	}

	validSortFields := map[string]bool{"name": true}
	sort := ctx.DefaultQuery("sort", "")
	order := strings.ToUpper(ctx.DefaultQuery("order", ""))

	if sort != "" && !validSortFields[sort] {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "sort", "invalid_sort")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("ListStoreRoles: Failed,error while loading service code")
		}
		errMap["sort"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_sort"},
		}
	}
	if order != "" && (order != "ASC" && order != "DESC") {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "order", "invalid_order")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("ListStoreRoles: Failed,error while loading service code")
		}
		errMap["order"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_order"},
		}
	}

	// Validate status parameter
	validStatusValues := map[string]bool{"active": true, "inactive": true, "all": true} // Add other valid values if needed
	status := ctx.DefaultQuery("status", "")
	if status != "" && !validStatusValues[status] {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "status", "invalid_status")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetAllStore: Failed,error while loading service code")
		}
		errMap["status"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_status"},
		}
	}

	// Validate is_live parameter
	validLiveValues := map[string]bool{"true": true, "false": true} // Add other valid values if needed
	isLive := ctx.DefaultQuery("live", "")
	if isLive != "" && !validLiveValues[isLive] {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, "live", "invalid_live")
		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetAllStore: Failed,error while loading service code")
		}
		errMap["live"] = models.ErrorResponse{
			Code:    code,
			Message: []string{"invalid_live"},
		}
	}

	for key, value := range errMap {
		serviceCode[key] = value.Code
	}

	if len(errMap) != 0 {
		for key, value := range errMap {
			serviceCode[key] = value.Code
		}
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			logger.Log().WithContext(ctxt).Error("GetStoreGenres failed: error occured while retrieving errors from localization module %s", val)
		}
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), val)

		ctx.JSON(http.StatusBadRequest, result)
		return
	}

	outletIDs := ctx.DefaultQuery("store_ids", "")
	input := strings.Trim(outletIDs, "[]")
	uuids := strings.Split(input, " ")

	// Joining the UUIDs with commas and enclosing them in quotes
	var formattedStrings []string
	for _, uuid := range uuids {
		formattedStrings = append(formattedStrings, "\""+uuid+"\"")
	}

	// Printing the output as a single string
	storeIdArr := fmt.Sprintf("[" + strings.Join(formattedStrings, ",") + "]")
	var storeIdSlice []string
	if len(storeIdArr) <= 0 {
		storeIdSlice = []string{}
	} else {
		var arrIds []string
		err := json.Unmarshal([]byte(storeIdArr), &arrIds)
		if err != nil {
			fmt.Println("Error:", err)
			return
		}

		storeIds := utilities.ConvertToStrings(arrIds)
		storeIdSlice = []string{storeIds}
	}

	var showAll bool
	if perPageVal == "" {
		showAll = true
	} else {
		showAll = false
	}

	// Use MetaDataInfo function from utils package
	response, err := store.useCases.GetStores(ctx, entities.Params{
		Q:        ctx.DefaultQuery("name", ""),
		Sort:     ctx.DefaultQuery("sort", ""),
		Order:    ctx.DefaultQuery("order", ""),
		Status:   ctx.DefaultQuery("status", ""),
		IsLive:   ctx.DefaultQuery("live", ""),
		ShowAll:  showAll,
		StoreIds: storeIdSlice,
		Limit:    perPageStr,
		Page:     pageStr,
	})

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetAllStore failed, Failed to get store")
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Errorf("GetAllStore failed, error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Errorf("GetAllStore failed, invalid endpoint %s", val)
		result := ErrorResponseGenerator(errDet.Message, int(errorCode), "")
		ctx.JSON(http.StatusBadRequest, result)
		return

	}

	// Construct the response using the ApiResponse struct
	data := entities.ResponseMetaData{
		Data:     response["stores"],
		MetaData: response["meta_data"],
	}

	var code int
	if stores, ok := response["stores"].([]entities.Store); ok {
		if len(stores) == 0 {
			code = http.StatusNoContent
		} else {
			code = http.StatusOK
		}
	}

	// Print the Total value
	resp := api.Response{
		Status:  "success",
		Message: "Store retrieved successfully",
		Code:    code,
		Data:    data,
		Errors:  struct{}{},
	}
	ctx.JSON(code, resp)

}

// GetSingleStore handles the HTTP request to get a single store by ID.
func (store *StoreController) GetSingleStore(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	storeIDStr := ctx.Param("store_id")
	storeID, _ := uuid.Parse(storeIDStr)
	helpLink := store.Cfg.ErrorHelpLink

	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	stores, err := store.useCases.GetStoreByID(ctx, storeID)
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("GetStoreById failed, Failed to get store")
		val, hasError, errorCode, _ := utils.ParseFields(ctx, consts.NotFound,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			logger.Log().WithContext(ctxt).Errorf("GetStoreById failed, error occured while retrieving errors from localization module %s", val)
		}
		logger.Log().WithContext(ctxt).Errorf("GetStoreById failed, invalid endpoint %s", val)
		result := ErrorResponseGenerator("Invalid Store Id", int(errorCode), "")
		ctx.JSON(int(errorCode), result)
		return
	}

	resp := api.Response{
		Status:  "success",
		Message: "Store retrieved successfully",
		Code:    http.StatusOK,
		Data:    stores,
		Errors:  struct{}{},
	}
	ctx.JSON(http.StatusOK, resp)
}
