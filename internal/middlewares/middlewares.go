package middlewares

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"store/config"
	"store/internal/consts"
	"store/internal/entities"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/patrickmn/go-cache"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// Middlewares represents a collection of middleware functions.
type Middlewares struct {
	Cache           *cache.Cache
	CacheExpiration int `default:"1" split_words:"true"`
	Cfg             *entities.EnvConfig
}

// NewMiddlewares creates a new instance of Middlewares.
func NewMiddlewares(cfg *entities.EnvConfig) *Middlewares {
	return &Middlewares{
		Cache: cache.New(5*time.Minute, 10*time.Minute),
		Cfg:   cfg,
	}
}

// APIVersioning is a middleware for handling API versioning.
func (m Middlewares) APIVersioning() gin.HandlerFunc {
	return func(c *gin.Context) {
		version := c.Param("version")
		if version == "" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Missing version parameter"})
			return
		}

		apiVersion := utils.PrepareVersionName(version)
		apiVersion = strings.ToUpper(apiVersion)

		// set the accepting version in the context
		c.Set(consts.AcceptedVersions, apiVersion)

		// init the system Accepted versions
		// init the env config
		cfg, err := config.LoadConfig(consts.AppName)
		if err != nil {
			panic(err)
		}

		// set the list of system accepting version in the context
		systemAcceptedVersionsList := cfg.AcceptedVersions

		for i, acceptVer := range cfg.AcceptedVersions {
			cfg.AcceptedVersions[i] = utils.PrepareVersionName(acceptVer)
		}

		c.Set(consts.ContextSystemAcceptedVersions, systemAcceptedVersionsList)

		// check the version exists in the accepted list
		// find index of version from Accepted versions
		var found bool
		for index, version := range systemAcceptedVersionsList {
			version = strings.ToUpper(version)
			if version == apiVersion {
				found = true
				c.Set(consts.ContextAcceptedVersionIndex, index)
			}

		}
		if !found {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Given version is not supported by the system"})
			return
		}

		c.Next()
	}
}

// LocalizationLanguage is a middleware for handling localization language.
func (m Middlewares) LocalizationLanguage() gin.HandlerFunc {
	return func(c *gin.Context) {
		// check it is exists in the header
		lan := c.Request.Header.Get(consts.HeaderLocallizationLanguage)
		if lan == "" {
			lan = "en"
		}
		// setting the language
		c.Set(consts.ContextLocalizationLanguage, lan)
		c.Next()
	}
}

// AuthorisationCheck is a middleware for handling authorization checks.
func (m Middlewares) AuthorisationCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		memberUUID := utils.GetHeader(c, "uuid")
		if memberUUID == "" {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "header value missing"})
			return
		}
		c.Next()
	}
}

// ErrorLocalization is a middleware for handling error localization.
func (m Middlewares) LocalizedError() gin.HandlerFunc {
	return func(c *gin.Context) {
		//for storing error response data from context
		var errorData = make(map[string]interface{})
		cacheData, isFound := m.Cache.Get(consts.CacheErrorData)
		//check whether data is in cache or not
		if isFound {
			log.Infof("found data in cache")
			c.Set(consts.ContextErrorResponses, cacheData)
		} else {
			localisationURL := fmt.Sprintf("%s/localization/error",
				m.Cfg.LocalisationServiceURL)
			log.Infof("[ErrorLocalization] Cache not found, calling downstream API")
			language, ok := utils.GetContext[string](c, consts.ContextLocalizationLanguage)
			if !ok {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": "Unable to find the localization language"})
				return
			}

			headers := map[string]interface{}{
				consts.HeaderLocallizationLanguage: language,
			}

			resp, err := utils.APIRequest(http.MethodGet, localisationURL, headers, nil)
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": "An unexpected error occured"})
				return
			}

			//checking the status code
			if resp.StatusCode != http.StatusOK {
				log.Errorf("unable to read data from localisation response %v", err)
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": "unable to read data from localisation response"})
				return
			}
			body, err := io.ReadAll(resp.Body)
			if err != nil {
				log.Errorf("unable to read data from localisation response %v", err)
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": fmt.Errorf("unable to read data from localisation response %v", err).Error()})
				return
			}
			defer resp.Body.Close()

			err = json.Unmarshal(body, &errorData)
			if err != nil {
				log.Errorf("unable to read data from localisation response %v", err)
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": "unable to read data from localisation response"})
				return
			}

			// set error data in context
			c.Set(consts.ContextErrorResponses, errorData)

			m.Cache.Set(consts.CacheErrorData, errorData,
				time.Duration(m.CacheExpiration)*time.Minute)
			// set error data in cache with an expiration time
			cacheExpiration, err := time.ParseDuration(fmt.Sprintf("%dm", m.CacheExpiration))
			if err != nil {
				log.Errorf("error parsing cache expiration duration: %v", err)
			} else {
				m.Cache.Set(consts.CacheErrorData, errorData, cacheExpiration)
			}
		}
		c.Next()
	}
}

// Middleware function to get endpointnames
func (m Middlewares) EndPointNames() gin.HandlerFunc {

	return func(c *gin.Context) {
		var endpoints models.ResponseData
		getEndpointUrl := fmt.Sprintf("%s/localization/endpointname",
			m.Cfg.EndpointURL)
		language, ok := utils.GetContext[string](c, consts.ContextLocallizationLanguage)
		if !ok {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "Unable to find the localization language"})
			return
		}

		headers := map[string]interface{}{
			consts.HeaderLocallizationLanguage: language,
		}

		resp, err := utils.APIRequest(http.MethodGet, getEndpointUrl, headers, nil)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "An unexpected error occured"})
			return
		}

		//checking the status code
		if resp.StatusCode != http.StatusOK {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "unable to read data from localisation response",
			})
			return
		}
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": fmt.Errorf("unable to read data from localisation response %v",
					err).Error(),
			})
			return
		}
		defer resp.Body.Close()

		err = json.Unmarshal(body, &endpoints)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "unable to read data from localisation response",
			})
			return
		}

		// set error data in context
		c.Set(consts.ContextEndPoints, endpoints)
		c.Next()
	}
}

func (m Middlewares) PartnerID() gin.HandlerFunc {
	return func(c *gin.Context) {
		partnerID := c.GetHeader("partner_id")
		if partnerID == "" {
			partnerID = consts.SampleTuneVersePartnerId
		}
		c.Set("partner_id", partnerID)
		c.Next()
	}
}
