package repo

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"reflect"
	"regexp"
	"store/internal/consts"
	"store/internal/entities"
	"strconv"
	"strings"

	"github.com/google/uuid"
	"github.com/labstack/gommon/log"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/utils"
)

// StoreRepo is a repository for handling store data.
type StoreRepo struct {
	db *sql.DB
}

// StoreRepoImply implements the StoreRepo interface.
type StoreRepoImply interface {
	//Stores
	CreateStore(ctx context.Context, newStore map[string]interface{}, variableName string) (string, error)
	GetStoreByID(ctx context.Context, id uuid.UUID) (*entities.Store, error)
	UpdateStore(ctx context.Context, storeID uuid.UUID, fields map[string]interface{}) error
	DeleteStore(ctx context.Context, storeID uuid.UUID) error
	GetStores(ctx context.Context, params entities.Params) ([]entities.Store, int, error)
	IsDuplicateStore(ctx context.Context, storeName string) (bool, error)
	IsDuplicateStorePatch(ctx context.Context, storeName string, storeId uuid.UUID) (bool, error)
	IsValidParentStore(ctx context.Context, ParentID string) (bool, error)

	//Store Genre
	AddGenresToStore(ctx context.Context, storeIngestionSpecID int) error
	GetStoreGenres(ctx context.Context, subQuery string, queryParam []entities.KeyValuePair, reqParam entities.ReqParams, storeId uuid.UUID) ([]entities.StoreGenres, int, error)
	IsStoreGenreExists(ctx context.Context, storeGenreId int64) (bool, error)
	EditStoreGenres(ctx context.Context, storeGenreId int64, data map[string]interface{}) error
	IsDuplicateStoreGenre(ctx context.Context, store_genre_name string, storeGenreId int64) (bool, error)
	IsValidGenreId(ctx context.Context, genre_id string) (bool, error)
	IsGenreIdExists(ctx context.Context, genre_id string) (bool, error)
	AddStoreGenre(ctx context.Context, data map[string]interface{}) ([]int64, error)
	DeleteStoreGenre(ctx context.Context, storeGenreID string) error

	//Store Role
	EditStoreRoles(ctx context.Context, storeRoleId int64, data map[string]interface{}) error
	IsDuplicateStoreRole(ctx context.Context, storeRoleName string, storeRoleId int64) (bool, error)
	IsStoreRoleExists(ctx context.Context, storeRoleId int64) (bool, error)
	AddRolesToStore(ctx context.Context, storeIngestionSpecID int) error
	GetStoreRoles(ctx context.Context, subQuery string, queryParam []entities.KeyValuePair, reqParam entities.ReqParams, storeId uuid.UUID) ([]entities.StoreRoles, int, error)
	AddStoreRole(ctx context.Context, data map[string]interface{}) ([]int64, error)
	DeleteStoreRole(ctx context.Context, storeGenreID string) error
	IsRoleIdExists(ctx context.Context, role_id string) (bool, error)
	IsValidRoleId(ctx context.Context, role_id string) (bool, error)

	//Store Ingestion Spec
	IsValidStoreIngestionSpecId(ctx context.Context, store_ingestion_spec_id int) (bool, error)
	GetAllStoreIngestionIds(ctx context.Context) ([]string, error)
	GetActiveStoreIngestionIds(ctx context.Context) ([]string, error)

	//Others
	IsVariableNameExists(ctx context.Context, variableName string) (bool, error)
	GenerateVariableName(ctx context.Context, storeName string) string
	IsValidReportingType(ctx context.Context, ReportingType string) (bool, error)
	GetReportingTypeValue(ctx context.Context, ReportingTypeId int) (string, error)
	IsValidDdexVersionId(ctx context.Context, ddex string) (bool, error)
	// GetActiveStores(ctx context.Context) ([]entities.ActiveStoreDetail, error)
	GetPartnerDetailById(ctxt context.Context, partnerID string) (map[string]interface{}, error)
}

// NewStoreRepo creates a new StoreRepo instance.
func NewStoreRepo(db *sql.DB) StoreRepoImply {
	return &StoreRepo{db: db}
}

func (repo *StoreRepo) GetPartnerDetailById(ctxt context.Context, partnerID string) (map[string]interface{}, error) {
	apiURL := fmt.Sprintf("%s/%s", consts.PartnerApiURL, partnerID)
	headers := make(map[string]interface{})
	headers["Content-Type"] = "application/json"

	response, err := utils.APIRequest("GET", apiURL, headers, nil)
	if err != nil {
		log.Printf("failed to make API request: %v", err)
		return nil, fmt.Errorf("failed to connect partner service")
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		log.Printf("non-200 response: %d", response.StatusCode)
		return nil, fmt.Errorf("unexpected status code: %d", response.StatusCode)
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Printf("failed to read response body: %v", err)
		return nil, err
	}

	var respData entities.PartnerApiResponse
	if err := json.Unmarshal(body, &respData); err != nil {
		log.Printf("Failed to unmarshal JSON: %v", err)
		return nil, fmt.Errorf("failed to fetch data from partner service")
	}

	data := make(map[string]interface{})
	data["name"] = respData.Data.Name
	return data, nil
}

// func (repo *StoreRepo) GetActiveStores(ctx context.Context) ([]entities.ActiveStoreDetail, error) {
// 	query := "SELECT COALESCE(s.id, '00000000-0000-0000-0000-000000000000'), COALESCE(s.name, '') FROM store s WHERE s.is_active = true"
// 	rows, err := repo.db.QueryContext(ctx, query)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer rows.Close()

// 	var stores []entities.ActiveStoreDetail
// 	for rows.Next() {
// 		var store entities.ActiveStoreDetail
// 		if err := rows.Scan(&store.ID, &store.Name); err != nil {
// 			return nil, err
// 		}
// 		stores = append(stores, store)
// 	}
// 	if err := rows.Err(); err != nil {
// 		return nil, err
// 	}
// 	return stores, nil
// }

func (repo *StoreRepo) GetActiveStoreIngestionIds(ctx context.Context) ([]string, error) {
	query := `SELECT si.id
	FROM store_ingestion_spec si
	JOIN store s ON si.store_id = s.id
	WHERE s.is_active = true`
	rows, err := repo.db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var storeIngestionIds []string
	for rows.Next() {
		var storeIngestionId string
		if err := rows.Scan(&storeIngestionId); err != nil {
			return nil, err
		}
		storeIngestionIds = append(storeIngestionIds, storeIngestionId)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return storeIngestionIds, nil
}

func (repo *StoreRepo) GetAllStoreIngestionIds(ctx context.Context) ([]string, error) {
	query := "SELECT id FROM store_ingestion_spec"
	rows, err := repo.db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var storeIngestionIds []string
	for rows.Next() {
		var storeIngestionId string
		if err := rows.Scan(&storeIngestionId); err != nil {
			return nil, err
		}
		storeIngestionIds = append(storeIngestionIds, storeIngestionId)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return storeIngestionIds, nil
}

func (repo *StoreRepo) IsStoreGenreExists(ctx context.Context, storeGenreId int64) (bool, error) {
	var count int
	err := repo.db.QueryRowContext(ctx, "SELECT COUNT(*) FROM store_genre WHERE id = $1", storeGenreId).Scan(&count)
	if err != nil {
		errMsg := fmt.Errorf("failed to check if store genre exists: %w", err)
		log.Errorf("IsStoreGenreExists failed, QueryRowContext() failed, err=%s", errMsg.Error())
		return false, errMsg
	}
	if count == 0 {
		errMsg := fmt.Errorf("store genre with ID %d does not exist", storeGenreId)
		log.Errorf("IsStoreGenreExists failed, %s", errMsg.Error())
		return false, errMsg
	}
	return true, nil
}

func (repo *StoreRepo) IsValidRoleId(ctx context.Context, role_id string) (bool, error) {
	var count int
	err := repo.db.QueryRowContext(ctx, "SELECT COUNT(*) FROM role WHERE id = $1", role_id).Scan(&count)
	if err != nil {
		errMsg := fmt.Errorf("failed to check if store role exists: %w", err)
		log.Errorf("IsValidRoleId failed, QueryRowContext() failed, err=%s", errMsg.Error())
		return false, errMsg
	}
	if count == 0 {
		errMsg := fmt.Errorf("role with ID %s does not exist", role_id)
		log.Errorf("IsValidRoleId failed, %s", errMsg.Error())
		return false, errMsg
	}

	return true, nil
}

func (repo *StoreRepo) IsRoleIdExists(ctx context.Context, role_id string) (bool, error) {
	var count int
	err := repo.db.QueryRowContext(ctx, "SELECT COUNT(*) FROM store_role WHERE role_id = $1", role_id).Scan(&count)
	if err != nil {
		errMsg := fmt.Errorf("failed to check if store role exists: %w", err)
		log.Errorf("IsValidRoleId failed, QueryRowContext() failed, err=%s", errMsg.Error())
		return false, errMsg
	}
	if count == 0 {
		errMsg := fmt.Errorf("role with ID %s does not exist", role_id)
		log.Errorf("IsValidRoleId failed, %s", errMsg.Error())
		return false, errMsg
	}

	return true, nil
}

func (repo *StoreRepo) IsGenreIdExists(ctx context.Context, genre_id string) (bool, error) {
	var count int
	err := repo.db.QueryRowContext(ctx, "SELECT COUNT(*) FROM store_genre WHERE genre_id = $1", genre_id).Scan(&count)
	if err != nil {
		errMsg := fmt.Errorf("failed to check if store genre exists: %w", err)
		log.Errorf("IsValidGenreId failed, QueryRowContext() failed, err=%s", errMsg.Error())
		return false, errMsg
	}
	if count == 0 {
		errMsg := fmt.Errorf("genre with ID %s does not exist", genre_id)
		log.Errorf("IsValidGenreId failed, %s", errMsg.Error())
		return false, errMsg
	}

	return true, nil
}

func (repo *StoreRepo) IsValidGenreId(ctx context.Context, genre_id string) (bool, error) {
	var count int
	err := repo.db.QueryRowContext(ctx, "SELECT COUNT(*) FROM genre WHERE id = $1", genre_id).Scan(&count)
	if err != nil {
		errMsg := fmt.Errorf("failed to check if store genre exists: %w", err)
		log.Errorf("IsValidGenreId failed, QueryRowContext() failed, err=%s", errMsg.Error())
		return false, errMsg
	}
	if count == 0 {
		errMsg := fmt.Errorf("genre with ID %s does not exist", genre_id)
		log.Errorf("IsValidGenreId failed, %s", errMsg.Error())
		return false, errMsg
	}

	return true, nil
}

func (repo *StoreRepo) IsValidStoreIngestionSpecId(ctx context.Context, store_ingestion_spec_id int) (bool, error) {
	var count int
	err := repo.db.QueryRowContext(ctx, "SELECT COUNT(*) FROM store_ingestion_spec WHERE id = $1", store_ingestion_spec_id).Scan(&count)
	if err != nil {
		errMsg := fmt.Errorf("failed to check if store genre exists: %w", err)
		log.Errorf("IsValidStoreIngestionSpecId failed, QueryRowContext() failed, err=%s", errMsg.Error())
		return false, errMsg
	}
	if count == 0 {
		errMsg := fmt.Errorf("StoreIngestionSpec with ID %d does not exist", store_ingestion_spec_id)
		log.Errorf("IsValidStoreIngestionSpecId failed, %s", errMsg.Error())
		return false, errMsg
	}

	return true, nil
}

func (repo *StoreRepo) IsStoreRoleExists(ctx context.Context, storeRoleId int64) (bool, error) {
	// Check if the record exists
	var count int
	err := repo.db.QueryRowContext(ctx, "SELECT COUNT(*) FROM store_role WHERE id = $1", storeRoleId).Scan(&count)
	if err != nil {
		errMsg := fmt.Errorf("failed to check if store role exists: %w", err)
		log.Errorf("IsStoreRoleExists failed, QueryRowContext() failed, err=%s", errMsg.Error())
		return false, errMsg
	}
	if count == 0 {
		errMsg := fmt.Errorf("store role with ID %d does not exist", storeRoleId)
		log.Errorf("IsStoreRoleExists failed, %s", errMsg.Error())
		return false, errMsg
	}

	return true, nil
}

func (repo *StoreRepo) AddStoreRole(ctx context.Context, data map[string]interface{}) ([]int64, error) {
	log := logger.Log().WithContext(ctx)

	if len(data) == 0 {
		return nil, errors.New("no data provided for insertion")
	}

	roleID, ok := data["role_id"].(string)
	if !ok {
		return nil, errors.New("role_id must be a string")
	}

	ingestionSpecIDInterface, ok := data["store_ingestion_spec_id"]
	if !ok || reflect.TypeOf(ingestionSpecIDInterface).Kind() != reflect.Slice {
		return nil, errors.New("store_ingestion_spec_id must be an array")
	}

	var ingestionSpecIDs []int
	switch reflect.ValueOf(ingestionSpecIDInterface).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(ingestionSpecIDInterface)
		for i := 0; i < s.Len(); i++ {
			el := s.Index(i).Interface()
			if num, ok := el.(float64); ok {
				ingestionSpecIDs = append(ingestionSpecIDs, int(num))
			} else if num, ok := el.(int); ok {
				ingestionSpecIDs = append(ingestionSpecIDs, num)
			} else {
				return nil, errors.New("store_ingestion_spec_id must contain only numeric values")
			}
		}
	default:
		return nil, errors.New("store_ingestion_spec_id must be an array of numbers")
	}

	var insertedIDs []int64
	for _, specID := range ingestionSpecIDs {
		fields := []string{"role_id", "store_ingestion_spec_id"}
		placeholders := []string{"$1", "$2"}
		values := []interface{}{roleID, specID}

		stmt := fmt.Sprintf("INSERT INTO store_role (%s) VALUES (%s) RETURNING id",
			strings.Join(fields, ", "), strings.Join(placeholders, ", "))

		var id int64
		err := repo.db.QueryRowContext(ctx, stmt, values...).Scan(&id)
		if err != nil {
			errMsg := fmt.Errorf("failed to insert into store_role: %w", err)
			log.Errorf("AddStoreRole failed, QueryRowContext() failed, err=%s", errMsg.Error())
			return nil, errMsg
		}
		insertedIDs = append(insertedIDs, id)
	}

	return insertedIDs, nil
}

func (repo *StoreRepo) AddStoreGenre(ctx context.Context, data map[string]interface{}) ([]int64, error) {
	log := logger.Log().WithContext(ctx)

	if len(data) == 0 {
		return nil, errors.New("no data provided for insertion")
	}

	roleID, ok := data["genre_id"].(string)
	if !ok {
		return nil, errors.New("genre_id must be a string")
	}

	ingestionSpecIDInterface, ok := data["store_ingestion_spec_id"]
	if !ok || reflect.TypeOf(ingestionSpecIDInterface).Kind() != reflect.Slice {
		return nil, errors.New("store_ingestion_spec_id must be an array")
	}

	var ingestionSpecIDs []int
	switch reflect.ValueOf(ingestionSpecIDInterface).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(ingestionSpecIDInterface)
		for i := 0; i < s.Len(); i++ {
			el := s.Index(i).Interface()
			if num, ok := el.(float64); ok {
				ingestionSpecIDs = append(ingestionSpecIDs, int(num))
			} else if num, ok := el.(int); ok {
				ingestionSpecIDs = append(ingestionSpecIDs, num)
			} else {
				return nil, errors.New("store_ingestion_spec_id must contain only numeric values")
			}
		}
	default:
		return nil, errors.New("store_ingestion_spec_id must be an array of numbers")
	}

	var insertedIDs []int64
	for _, specID := range ingestionSpecIDs {
		fields := []string{"genre_id", "store_ingestion_spec_id"}
		placeholders := []string{"$1", "$2"}
		values := []interface{}{roleID, specID}

		stmt := fmt.Sprintf("INSERT INTO store_genre (%s) VALUES (%s) RETURNING id",
			strings.Join(fields, ", "), strings.Join(placeholders, ", "))

		var id int64
		err := repo.db.QueryRowContext(ctx, stmt, values...).Scan(&id)
		if err != nil {
			errMsg := fmt.Errorf("failed to insert into store_genre: %w", err)
			log.Errorf("AddStoreGenre failed, QueryRowContext() failed, err=%s", errMsg.Error())
			return nil, errMsg
		}
		insertedIDs = append(insertedIDs, id)
	}

	return insertedIDs, nil
}

func (repo *StoreRepo) EditStoreGenres(ctx context.Context, storeGenreId int64, data map[string]interface{}) error {
	log := logger.Log().WithContext(ctx)
	if len(data) == 0 {
		return nil
	}

	if storeGenre, ok := data["store_genre"]; ok {
		data["store_genre_name"] = storeGenre
		delete(data, "store_genre")
	}
	var updates []string
	var values []interface{}
	index := 1
	for field, value := range data {
		updates = append(updates, fmt.Sprintf("%s = $%d", field, index))
		values = append(values, value)
		index++
	}

	values = append(values, storeGenreId)

	stmt := fmt.Sprintf("UPDATE store_genre SET %s WHERE id = $%d",
		strings.Join(updates, ", "), index)

	_, err := repo.db.ExecContext(ctx, stmt, values...)
	if err != nil {
		errMsg := fmt.Errorf("failed to update store genre fields: %w", err)
		log.Errorf("EditStoreGenres failed, ExecContext() failed, err=%s", errMsg.Error())
		return errMsg
	}

	return nil
}

func (repo *StoreRepo) EditStoreRoles(ctx context.Context, storeRoleId int64, data map[string]interface{}) error {
	log := logger.Log().WithContext(ctx)
	if len(data) == 0 {
		return nil
	}
	// Map "store_role" to "store_role_name" if present in the data
	if storeRole, ok := data["store_role"]; ok {
		data["store_role_name"] = storeRole
		delete(data, "store_role")
	}
	var updates []string
	var values []interface{}
	index := 1
	for field, value := range data {
		updates = append(updates, fmt.Sprintf("%s = $%d", field, index))
		values = append(values, value)
		index++
	}

	values = append(values, storeRoleId)

	stmt := fmt.Sprintf("UPDATE store_role SET %s WHERE id = $%d",
		strings.Join(updates, ", "), index)

	_, err := repo.db.ExecContext(ctx, stmt, values...)
	if err != nil {
		errMsg := fmt.Errorf("failed to update store role fields: %w", err)
		log.Errorf("EditStoreRoles failed, ExecContext() failed, err=%s", errMsg.Error())
		return errMsg
	}

	return nil
}

func (repo *StoreRepo) GetStoreGenres(ctx context.Context, subQuery string, queryParam []entities.KeyValuePair, reqParam entities.ReqParams, storeId uuid.UUID) ([]entities.StoreGenres, int, error) {
	var storeGenres []entities.StoreGenres
	var queryParams []interface{}

	fieldMap := map[string]string{
		"store_genre": "pt.store_genre_name",
		"genre":       "ge.name",
		"store_name":  "st.name",
	}
	sort := fieldMap[reqParam.Sort]
	if sort == "" {
		sort = "pt.store_genre_name"
	}
	order := reqParam.Order
	if order == "" {
		order = "ASC"
	}

	var whereClauses []string
	for _, param := range queryParam {
		if param.Value == "" {
			continue
		}

		key := fieldMap[param.Key]
		if key == "" {
			continue
		}

		operator := "="
		if strings.HasPrefix(param.Value, "~") {
			operator = "ILIKE"
			param.Value = "%" + param.Value[1:] + "%"
		}
		placeholder := len(queryParams) + 1
		whereClauses = append(whereClauses, fmt.Sprintf("LOWER(%s) %s $%d", key, operator, placeholder))
		queryParams = append(queryParams, strings.ToLower(param.Value))
	}

	var whereClause string
	if len(whereClauses) > 0 {
		whereClause = " AND " + strings.Join(whereClauses, " AND ")
	}

	whereClause += fmt.Sprintf(" AND st.id = $%d", len(queryParams)+1)
	queryParams = append(queryParams, storeId)

	query := `
        SELECT pt.id, pt.genre_id, ge.name as genre_name, COALESCE(pt.store_genre_name, ''), st.name as store_name, pt.store_ingestion_spec_id
        FROM store_genre pt
        JOIN genre ge ON pt.genre_id = ge.id
        JOIN store_ingestion_spec si ON pt.store_ingestion_spec_id = si.id 
        JOIN store st ON si.store_id = st.id
        WHERE st.is_active = true` + whereClause + `
        ORDER BY ` + sort + ` ` + order + ` ` + subQuery

	rows, err := repo.db.QueryContext(ctx, query, queryParams...)
	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	for rows.Next() {
		var storeGenre entities.StoreGenres
		if err := rows.Scan(&storeGenre.ID, &storeGenre.GenreID, &storeGenre.GenreName, &storeGenre.StoreGenreName,
			&storeGenre.StoreName, &storeGenre.StoreIngestionSpecID); err != nil {
			return nil, 0, err
		}
		storeGenres = append(storeGenres, storeGenre)
	}

	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	countQuery := `
        SELECT COUNT(*) 
        FROM store_genre pt
        JOIN genre ge ON pt.genre_id = ge.id
        JOIN store_ingestion_spec si ON pt.store_ingestion_spec_id = si.id 
        JOIN store st ON si.store_id = st.id
        WHERE st.is_active = true` + whereClause

	var totalCount int
	err = repo.db.QueryRowContext(ctx, countQuery, queryParams...).Scan(&totalCount)
	if err != nil {
		return nil, 0, err
	}

	return storeGenres, totalCount, nil
}

func (repo *StoreRepo) GetStoreRoles(ctx context.Context, subQuery string, queryParam []entities.KeyValuePair, reqParam entities.ReqParams, storeId uuid.UUID) ([]entities.StoreRoles, int, error) {
	var storeRoles []entities.StoreRoles
	var queryParams []interface{}

	fieldMap := map[string]string{
		"store_role": "pt.store_role_name",
		"role":       "ge.name",
		"store_name": "st.name",
	}
	sort := fieldMap[reqParam.Sort]
	if sort == "" {
		sort = "pt.store_role_name"
	}
	order := reqParam.Order
	if order == "" {
		order = "ASC"
	}

	var whereClauses []string
	for _, param := range queryParam {
		if param.Value == "" {
			continue
		}

		key := fieldMap[param.Key]
		if key == "" {
			continue
		}

		operator := "="
		if strings.HasPrefix(param.Value, "~") {
			operator = "ILIKE"
			param.Value = "%" + param.Value[1:] + "%"
		}
		placeholder := len(queryParams) + 1
		whereClauses = append(whereClauses, fmt.Sprintf("LOWER(%s) %s $%d", key, operator, placeholder))
		queryParams = append(queryParams, strings.ToLower(param.Value))
	}

	var whereClause string
	if len(whereClauses) > 0 {
		whereClause = " AND " + strings.Join(whereClauses, " AND ")
	}

	whereClause += fmt.Sprintf(" AND st.id = $%d", len(queryParams)+1)
	queryParams = append(queryParams, storeId)

	query := `
		SELECT pt.id, pt.role_id ,ge.name as role_name ,  COALESCE(pt.store_role_name, ''), st.name as store_name , pt.store_ingestion_spec_id,
		pt.is_user_defined, pt.is_resource_contributor
		FROM store_role pt
		JOIN role ge ON pt.role_id = ge.id
		JOIN store_ingestion_spec si ON pt.store_ingestion_spec_id = si.id 
		JOIN store st ON si.store_id = st.id
		WHERE st.is_active = true ` + whereClause + `
        ORDER BY ` + sort + ` ` + order + ` ` + subQuery

	rows, err := repo.db.QueryContext(ctx, query, queryParams...)
	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	for rows.Next() {
		var storeRole entities.StoreRoles
		if err := rows.Scan(&storeRole.ID, &storeRole.RoleID, &storeRole.RoleName, &storeRole.StoreRoleName,
			&storeRole.StoreName, &storeRole.StoreIngestionSpecID, &storeRole.IsUserDefined, &storeRole.IsResourceContributor); err != nil {
			return nil, 0, err
		}
		storeRoles = append(storeRoles, storeRole)
	}

	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	countQuery := `
        SELECT COUNT(*) 
		FROM store_role pt
		JOIN role ge ON pt.role_id = ge.id
		JOIN store_ingestion_spec si ON pt.store_ingestion_spec_id = si.id 
		JOIN store st ON si.store_id = st.id
		WHERE st.is_active = true ` + whereClause

	var totalCount int
	err = repo.db.QueryRowContext(ctx, countQuery, queryParams...).Scan(&totalCount)
	if err != nil {
		return nil, 0, err
	}

	return storeRoles, totalCount, nil
}

func (repo *StoreRepo) IsDuplicateStore(ctx context.Context, storeName string) (bool, error) {
	query := "SELECT COUNT(*) FROM store WHERE LOWER(name) = LOWER($1)"

	var count int
	err := repo.db.QueryRowContext(ctx, query, storeName).Scan(&count)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}

func (repo *StoreRepo) IsDuplicateStorePatch(ctx context.Context, storeName string, storeId uuid.UUID) (bool, error) {
	query := "SELECT COUNT(*) FROM store WHERE LOWER(name) = LOWER($1) AND id != $2"

	var count int
	err := repo.db.QueryRowContext(ctx, query, storeName, storeId).Scan(&count)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}

func (repo *StoreRepo) IsDuplicateStoreGenre(ctx context.Context, store_genre_name string, storeGenreId int64) (bool, error) {
	query := "SELECT COUNT(*) FROM store_genre WHERE store_genre_name = $1 AND id != $2"

	var count int
	err := repo.db.QueryRowContext(ctx, query, store_genre_name, storeGenreId).Scan(&count)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}

func (repo *StoreRepo) IsDuplicateStoreRole(ctx context.Context, storeRoleName string, storeRoleId int64) (bool, error) {
	query := "SELECT COUNT(*) FROM store_role WHERE store_role_name = $1 AND id != $2"

	var count int
	err := repo.db.QueryRowContext(ctx, query, storeRoleName, storeRoleId).Scan(&count)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}

func (repo *StoreRepo) IsVariableNameExists(ctx context.Context, variableName string) (bool, error) {
	query := "SELECT COUNT(*) FROM store WHERE variable_name = $1"

	var count int
	err := repo.db.QueryRowContext(ctx, query, variableName).Scan(&count)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}

func (repo *StoreRepo) IsValidParentStore(ctx context.Context, ParentID string) (bool, error) {
	if ParentID == "" {
		return true, nil
	} else {
		query := "SELECT COUNT(*) FROM store WHERE id = $1"

		var count int
		err := repo.db.QueryRowContext(ctx, query, ParentID).Scan(&count)
		if err != nil {
			return false, err
		}

		return count > 0, nil
	}
}

func (repo *StoreRepo) IsValidReportingType(ctx context.Context, ReportingType string) (bool, error) {
	apiURL := fmt.Sprintf("%s/%d/%s", consts.LookupApiURL, consts.ReportingTypeId, ReportingType)
	headers := make(map[string]interface{})
	headers["Content-Type"] = "application/json"

	response, err := utils.APIRequest("HEAD", apiURL, headers, nil)
	if err != nil {
		log.Printf("failed to make API request: %v", err)
		return false, fmt.Errorf("failed to connect lookup service")
	}
	defer response.Body.Close()

	if response.StatusCode == http.StatusOK {
		return true, nil
	} else {
		return false, nil
	}
}

func (repo *StoreRepo) IsValidDdexVersionId(ctx context.Context, ddex string) (bool, error) {
	// ddexStringValue := strconv.FormatFloat(ddex, 'f', -1, 64)
	query := "SELECT EXISTS(SELECT 1 FROM lookup WHERE lookup_type_id = $1 AND value = $2)"
	var exists bool
	err := repo.db.QueryRowContext(ctx, query, 4, ddex).Scan(&exists)
	if err != nil {
		return false, err
	}
	return exists, nil
}

func (repo *StoreRepo) GetReportingTypeValue(ctx context.Context, ReportingTypeId int) (string, error) {

	query := "SELECT value FROM lookup WHERE lookup_type_id = $1 AND id = $2"
	var value string
	err := repo.db.QueryRowContext(ctx, query, 22, ReportingTypeId).Scan(&value)
	if err != nil {
		return "", err
	}

	return value, nil

}

func (repo *StoreRepo) GenerateVariableName(ctx context.Context, storeName string) string {
	re := regexp.MustCompile(`[^a-zA-Z0-9]+`)
	words := re.Split(storeName, -1)

	for i, word := range words {
		if i == 0 {
			// Lowercase the first word
			words[i] = strings.ToLower(word)
		} else {
			// Capitalize subsequent words
			words[i] = strings.Title(word)
		}
	}

	return strings.Join(words, "")
}

func (repo *StoreRepo) CreateStore(ctx context.Context, fields map[string]interface{}, variableName string) (string, error) {
	if len(fields) == 0 {
		return "", nil // No fields to insert
	}

	expectedKeys := map[string]string{
		"live":                      "is_live",
		"active":                    "is_active",
		"featurefm_enabled":         "is_featurefm_enabled",
		"dolby_enabled":             "is_dolby_enabled",
		"stereo_required_for_dolby": "is_stereo_required_for_dolby",
		"sony_enabled":              "is_sony_enabled",
		"price_code_enabled":        "is_price_code_enabled",
		"pre_save_enabled":          "is_pre_save_enabled",
		"default":                   "is_default",
		"locked":                    "is_locked",
	}

	var fieldNames []string
	var placeholders []string
	var values []interface{}
	index := 1

	for key, value := range fields {
		if value == nil || value == "" {
			// Skip nil or empty values
			continue
		}

		// Get the mapped column name from expectedKeys if it exists, otherwise use the original key
		columnName := expectedKeys[key]
		if columnName == "" {
			columnName = key
		}

		fieldNames = append(fieldNames, columnName)
		placeholders = append(placeholders, fmt.Sprintf("$%d", index))
		values = append(values, value)
		index++
	}

	// Construct the SQL insert statement
	stmt := fmt.Sprintf("INSERT INTO store (%s) VALUES (%s) RETURNING id",
		strings.Join(fieldNames, ", "), strings.Join(placeholders, ", "))

	// Print or log the generated SQL statement for debugging
	fmt.Println("Generated SQL:", stmt)

	// Executing the SQL insert statement
	var insertedID string
	err := repo.db.QueryRowContext(ctx, stmt, values...).Scan(&insertedID)
	if err != nil {
		return "", fmt.Errorf("failed to add %s: %w", variableName, err)
	}

	// Other operations after successful insertion
	// For example, updating variable name
	updateStmt := "UPDATE store SET variable_name = $1 WHERE id = $2"
	_, err = repo.db.ExecContext(ctx, updateStmt, variableName, insertedID)
	if err != nil {
		return "", fmt.Errorf("failed to update variableName: %w", err)
	}

	// Inserting into another table
	var storeIngestionID int
	storeIngestionQuery := "INSERT INTO store_ingestion_spec (store_id) VALUES ($1) RETURNING id"
	err = repo.db.QueryRowContext(ctx, storeIngestionQuery, insertedID).Scan(&storeIngestionID)
	if err != nil {
		return "", fmt.Errorf("failed to insert store ingestion spec: %w", err)
	}

	// Additional operations such as adding genres and roles to the store
	if err := repo.AddGenresToStore(ctx, storeIngestionID); err != nil {
		return "", fmt.Errorf("failed to add genres to store: %v", err)
	}

	if err := repo.AddRolesToStore(ctx, storeIngestionID); err != nil {
		return "", fmt.Errorf("failed to add roles to store: %v", err)
	}

	return insertedID, nil
}

func (repo *StoreRepo) UpdateStore(ctx context.Context, storeID uuid.UUID, fields map[string]interface{}) error {
	if len(fields) == 0 {
		return nil
	}

	var storeIngestionSpecID int // Assuming it's an integer
	err := repo.db.QueryRowContext(ctx, "SELECT id FROM store_ingestion_spec WHERE store_id = $1", storeID).Scan(&storeIngestionSpecID)
	if err != nil {
		storeIngestionQuery := "INSERT INTO store_ingestion_spec (store_id) VALUES ($1) RETURNING id"
		err = repo.db.QueryRowContext(ctx, storeIngestionQuery, storeID).Scan(&storeIngestionSpecID)
		if err != nil {
			return fmt.Errorf("failed to insert store ingestion spec: %w", err)
		}
	}

	expectedKeys := map[string]string{
		"live":                      "is_live",
		"active":                    "is_active",
		"featurefm_enabled":         "is_featurefm_enabled",
		"dolby_enabled":             "is_dolby_enabled",
		"stereo_required_for_dolby": "is_stereo_required_for_dolby",
		"sony_enabled":              "is_sony_enabled",
		"price_code_enabled":        "is_price_code_enabled",
		"pre_save_enabled":          "is_pre_save_enabled",
		"default":                   "is_default",
		"locked":                    "is_locked",
	}

	// Construct the SQL update statement dynamically
	var updates []string
	var values []interface{}
	index := 1

	for field, value := range fields {
		if renamedField, ok := expectedKeys[field]; ok {
			field = renamedField
		}

		if field == "parent_id" {
			if value == "" {
				// Handle empty parent_id value (assuming it's of type UUID)
				updates = append(updates, fmt.Sprintf("%s = $%d", field, index))
				values = append(values, nil)
			} else {
				updates = append(updates, fmt.Sprintf("%s = $%d", field, index))
				values = append(values, value)
			}
		} else if value != "" {
			updates = append(updates, fmt.Sprintf("%s = $%d", field, index))
			values = append(values, value)
		}

		index++
	}

	// Add the storeID as the last value
	values = append(values, storeID)
	stmt := fmt.Sprintf("UPDATE store SET %s WHERE id = $%d", strings.Join(updates, ", "), index)

	// Print or log the generated SQL statement for debugging
	fmt.Println("Generated SQL:", stmt)

	// Begin a transaction
	tx, err := repo.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to begin transaction: %w", err)
	}
	defer tx.Rollback() // Rollback if the transaction is not committed

	_, err = repo.db.ExecContext(ctx, stmt, values...)
	if err != nil {
		return fmt.Errorf("failed to update store fields: %w", err)
	}

	if active, ok := fields["active"].(bool); ok {
		if !active {
			deleteStoreGenre := "DELETE FROM store_genre WHERE store_ingestion_spec_id = $1"
			_, err = repo.db.ExecContext(ctx, deleteStoreGenre, storeIngestionSpecID)
			if err != nil {
				return fmt.Errorf("failed to update variableName: %w", err)
			}
			deleteStoreRole := "DELETE FROM store_role WHERE store_ingestion_spec_id = $1"
			_, err = repo.db.ExecContext(ctx, deleteStoreRole, storeIngestionSpecID)
			if err != nil {
				return fmt.Errorf("failed to update variableName: %w", err)
			}

		} else {
			deleteStoreGenre := "DELETE FROM store_genre WHERE store_ingestion_spec_id = $1"
			_, err = repo.db.ExecContext(ctx, deleteStoreGenre, storeIngestionSpecID)
			if err != nil {
				return fmt.Errorf("failed to update variableName: %w", err)
			}
			deleteStoreRole := "DELETE FROM store_role WHERE store_ingestion_spec_id = $1"
			_, err = repo.db.ExecContext(ctx, deleteStoreRole, storeIngestionSpecID)
			if err != nil {
				return fmt.Errorf("failed to update variableName: %w", err)
			}
			repo.AddGenresToStore(ctx, storeIngestionSpecID)
			err = repo.AddRolesToStore(ctx, storeIngestionSpecID)
			if err != nil {
				return fmt.Errorf("failed to insert roles: %w", err)
			}
		}
	}

	return nil
}

func (repo *StoreRepo) AddGenresToStore(ctx context.Context, storeIngestionSpecID int) error {
	// Query all genres from the genre table
	rows, err := repo.db.QueryContext(ctx, "SELECT id, name FROM genre WHERE is_deleted = false")
	if err != nil {
		return fmt.Errorf("failed to fetch genres: %w", err)
	}
	defer rows.Close()

	// Prepare the SQL statement for inserting into the store_genre table
	stmt, err := repo.db.PrepareContext(ctx, "INSERT INTO store_genre (genre_id, store_genre_name, store_ingestion_spec_id) VALUES ($1, $2, $3)")
	if err != nil {
		return fmt.Errorf("failed to prepare statement: %w", err)
	}
	defer stmt.Close()

	// Iterate over the rows and insert each genre into the store_genre table
	for rows.Next() {
		var genreID uuid.UUID
		var genreName string
		if err := rows.Scan(&genreID, &genreName); err != nil {
			return fmt.Errorf("failed to scan row: %w", err)
		}

		// Insert the genre into the store_genre table
		_, err := stmt.ExecContext(ctx, genreID, genreName, storeIngestionSpecID)
		if err != nil {
			return fmt.Errorf("failed to insert genre into store_genre table: %w", err)
		}
	}

	if err := rows.Err(); err != nil {
		return fmt.Errorf("error iterating over rows: %w", err)
	}

	return nil
}

func (repo *StoreRepo) AddRolesToStore(ctx context.Context, storeIngestionSpecID int) error {
	// Query all roles from the role table
	rows, err := repo.db.QueryContext(ctx, "SELECT id, name FROM role WHERE is_deleted = false")
	if err != nil {
		return fmt.Errorf("failed to fetch roles: %w", err)
	}
	defer rows.Close()

	// Prepare the SQL statement for inserting into the store_role table
	stmt, err := repo.db.PrepareContext(ctx, "INSERT INTO store_role (role_id, store_role_name, store_ingestion_spec_id) VALUES ($1, $2, $3)")
	if err != nil {
		return fmt.Errorf("failed to prepare statement: %w", err)
	}
	defer stmt.Close()

	// Iterate over the rows and insert each role into the store_role table
	for rows.Next() {
		var roleID uuid.UUID
		var roleName string
		if err := rows.Scan(&roleID, &roleName); err != nil {
			return fmt.Errorf("failed to scan row: %w", err)
		}

		// Insert the role into the store_role table
		_, err := stmt.ExecContext(ctx, roleID, roleName, storeIngestionSpecID)
		if err != nil {
			return fmt.Errorf("failed to insert role into store_role table: %w", err)
		}
	}

	if err := rows.Err(); err != nil {
		return fmt.Errorf("error iterating over rows: %w", err)
	}

	return nil
}

// GetStoreByID retrieves a store by its ID.
func (repo *StoreRepo) GetStoreByID(ctx context.Context, id uuid.UUID) (*entities.Store, error) {

	query := `
	SELECT id, COALESCE(name, ''), COALESCE(parent_id::text, ''), COALESCE(remote_path, ''), is_active, is_live, use_generic_package, COALESCE(variable_name, ''),
	COALESCE(reporting_type, ''), featurefm_name, is_featurefm_enabled, process_duration_days, ddex_version_id,
	crs_permission_id, is_dolby_enabled, dolby_codec_id, is_stereo_required_for_dolby, is_sony_enabled,
	preferred_provider_program, include_featured_artist, is_price_code_enabled, is_pre_save_enabled,
	is_default, COALESCE(logo, ''), is_locked
	FROM store
	WHERE id = $1 
	AND (is_active = true OR is_live = true)
	;
	`
	row := repo.db.QueryRowContext(ctx, query, id)
	var store entities.Store

	err := row.Scan(
		&store.ID, &store.Name, &store.ParentID, &store.RemotePath, &store.IsActive, &store.IsLive,
		&store.UseGenericPackage, &store.VariableName, &store.ReportingType, &store.FeatureFMName,
		&store.IsFeatureFMEnabled, &store.ProcessDurationDays, &store.DDEXVersionID,
		&store.CRSPermissionID, &store.IsDolbyEnabled, &store.DolbyCodecID,
		&store.IsStereoRequiredForDolby, &store.IsSonyEnabled, &store.PreferredProviderProgram,
		&store.IncludeFeaturedArtist, &store.IsPriceCodeEnabled, &store.IsPreSaveEnabled,
		&store.IsDefault, &store.Logo, &store.IsLocked,
	)

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, errors.New("store not found")
		}
		return nil, fmt.Errorf("failed to fetch store: %w", err)
	}

	return &store, nil
}

// DeleteStore deletes a store by its ID.
func (repo *StoreRepo) DeleteStore(ctx context.Context, storeID uuid.UUID) error {
	// Check if the store exists before deleting
	_, err := repo.GetStoreByID(ctx, storeID)
	if err != nil {
		fmt.Println(err)
		if errors.Is(err, sql.ErrNoRows) {
			return errors.New("store not found") // Update the error message
		}
		return fmt.Errorf("failed to check store existence: %w", err)
	}

	// Delete the store
	query := `UPDATE store SET is_live = false, is_active = false WHERE id = $1`
	_, err = repo.db.ExecContext(ctx, query, storeID)
	if err != nil {
		return fmt.Errorf("failed to soft delete store: %w", err)
	}
	return nil
}

func (repo *StoreRepo) DeleteStoreGenre(ctx context.Context, storeGenreID string) error {
	query := `DELETE FROM store_genre WHERE genre_id = $1`
	_, err := repo.db.ExecContext(ctx, query, storeGenreID)
	if err != nil {
		return fmt.Errorf("failed to delete store genre: %w", err)
	}

	return nil
}

func (repo *StoreRepo) DeleteStoreRole(ctx context.Context, storeGenreID string) error {
	query := `DELETE FROM store_role WHERE role_id = $1`
	_, err := repo.db.ExecContext(ctx, query, storeGenreID)
	if err != nil {
		return fmt.Errorf("failed to delete store role: %w", err)
	}

	return nil
}

func (repo *StoreRepo) GetStores(ctx context.Context, params entities.Params) ([]entities.Store, int, error) {
	var stores []entities.Store
	var searchQuery string
	var sort string
	var order string
	var status string
	var is_live string
	var storeIDsClause string

	if len(params.StoreIds) > 0 {
		storeIDsClause = " AND id IN ("
		for i, id := range params.StoreIds {
			if len(strings.TrimSpace(id)) > 0 {
				storeIDsClause += id
				if i < len(params.StoreIds)-1 {
					storeIDsClause += ","
				}
			}
		}
		storeIDsClause += ")"
	} else {
		storeIDsClause = ""
	}

	cleanedQ := strings.TrimPrefix(params.Q, "~")

	if strings.HasPrefix(params.Q, "~") {
		searchQuery = "AND name ILIKE '%' || COALESCE($3, '') || '%'"
	} else if params.Q != "" {
		searchQuery = "AND LOWER(name) = LOWER(COALESCE(CAST($3 AS text), ''))"
	}

	order = params.Order
	if order == "" {
		order = "ASC"
	}

	sort = params.Sort
	if sort == "" {
		sort = "name"
	}

	status = params.Status
	if status == "all" {
		status = " AND is_active IN (true, false)"
	} else if status == "inactive" {
		status = " AND is_active = false"
	} else {
		status = " AND is_active = true"
	}

	is_live = params.IsLive
	if is_live == "true" {
		is_live = " = true"
	} else if is_live == "false" {
		is_live = " = false"
	} else {
		is_live = " IN (true, false)"
	}

	query := `
    SELECT id, COALESCE(name, ''), COALESCE(parent_id::text, ''), COALESCE(remote_path, ''), is_active, is_live, use_generic_package, COALESCE(variable_name, ''),
    COALESCE(reporting_type, ''), COALESCE(featurefm_name, ''), is_featurefm_enabled, process_duration_days, ddex_version_id,
    crs_permission_id, is_dolby_enabled, dolby_codec_id, is_stereo_required_for_dolby, is_sony_enabled,
    preferred_provider_program, include_featured_artist, is_price_code_enabled, is_pre_save_enabled,
    is_default, COALESCE(logo, ''), is_locked
    FROM store
    WHERE is_live` + is_live + status + ` ` + searchQuery + storeIDsClause + `
    ORDER BY ` + sort + ` ` + order + `
    LIMIT $1 OFFSET $2;
    `

	limitInt, err := strconv.Atoi(params.Limit)
	if err != nil {
		return nil, 0, fmt.Errorf("failed to convert limit to int: %w", err)
	}

	pageInt, err := strconv.Atoi(params.Page)
	if err != nil {
		return nil, 0, fmt.Errorf("failed to convert page to int: %w", err)
	}

	offset := (pageInt - 1) * limitInt
	var rows *sql.Rows
	if params.Q != "" {
		rows, err = repo.db.QueryContext(ctx, query, limitInt, offset, cleanedQ)
	} else {
		rows, err = repo.db.QueryContext(ctx, query, limitInt, offset)
	}
	if err != nil {
		return nil, 0, fmt.Errorf("failed to fetch stores: %w", err)
	}
	defer rows.Close()

	for rows.Next() {
		var store entities.Store

		err := rows.Scan(
			&store.ID, &store.Name, &store.ParentID, &store.RemotePath, &store.IsActive, &store.IsLive,
			&store.UseGenericPackage, &store.VariableName, &store.ReportingType, &store.FeatureFMName,
			&store.IsFeatureFMEnabled, &store.ProcessDurationDays, &store.DDEXVersionID,
			&store.CRSPermissionID, &store.IsDolbyEnabled, &store.DolbyCodecID,
			&store.IsStereoRequiredForDolby, &store.IsSonyEnabled, &store.PreferredProviderProgram,
			&store.IncludeFeaturedArtist, &store.IsPriceCodeEnabled, &store.IsPreSaveEnabled,
			&store.IsDefault, &store.Logo, &store.IsLocked,
		)

		if err != nil {

			return nil, 0, fmt.Errorf("failed to scan store row: %w", err)
		}

		stores = append(stores, store)
	}

	var totalCount int
	var countQuery string
	if strings.HasPrefix(params.Q, "~") {
		countQuery = `SELECT COUNT(*) FROM store WHERE name ILIKE '%' || COALESCE($1, '') || '%' AND is_live` + is_live + status + storeIDsClause + `;`
		err = repo.db.QueryRowContext(ctx, countQuery, cleanedQ).Scan(&totalCount)
	} else if params.Q != "" {
		countQuery = `SELECT COUNT(*) FROM store WHERE LOWER(name) = LOWER(COALESCE(CAST($1 AS text), '')) AND is_live` + is_live + status + storeIDsClause + `;`
		err = repo.db.QueryRowContext(ctx, countQuery, cleanedQ).Scan(&totalCount)
	} else {
		countQuery = `SELECT COUNT(*) FROM store WHERE is_live` + is_live + status + storeIDsClause + `;`
		err = repo.db.QueryRowContext(ctx, countQuery).Scan(&totalCount)
	}

	if err != nil {
		return nil, 0, fmt.Errorf("failed to count stores: %w", err)
	}

	return stores, totalCount, nil
}
