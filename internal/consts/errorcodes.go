package consts

var ErrorCodeMap = map[string]interface{}{
	"store_role": map[string]interface{}{
		"get": map[string]interface{}{
			"page": map[string]interface{}{
				"invalid_page": "A1001",
			},
			"limit": map[string]interface{}{
				"invalid_limit": "A1002",
				"maxlimit":      "A1003",
			},
			"sort": map[string]interface{}{
				"invalid_sort": "A1004",
			},
			"order": map[string]interface{}{
				"invalid_order": "A1005",
			},
		},
		"patch": map[string]interface{}{
			"store_role": map[string]interface{}{
				"required": "A1006",
				"min":      "A1007",
				"max":      "A1008",
				"invalid":  "A1009",
				"exists":   "A1010",
			},
			"common_messages": map[string]interface{}{
				"notfound": "A1010",
			},
		},
		"post": map[string]interface{}{
			"role_id": map[string]interface{}{
				"required": "A1016",
				"invalid":  "A1019",
			},
			"store_ingestion_spec_id": map[string]interface{}{
				"required": "A1016",
				"invalid":  "A1019"},
		},
		"delete": map[string]interface{}{
			"role_id": map[string]interface{}{
				"invalid": "A1019",
			},
		},
	},
	"store_genre": map[string]interface{}{
		"get": map[string]interface{}{
			"page": map[string]interface{}{
				"invalid_page": "A1011",
			},
			"limit": map[string]interface{}{
				"invalid_limit": "A1012",
				"maxlimit":      "A1013",
			},
			"sort": map[string]interface{}{
				"invalid_sort": "A1014",
			},
			"order": map[string]interface{}{
				"invalid_order": "A1015",
			},
		},
		"patch": map[string]interface{}{
			"store_genre": map[string]interface{}{
				"required": "A1016",
				"min":      "A1017",
				"max":      "A1018",
				"invalid":  "A1019",
				"exists":   "A1020",
			},
			"common_messages": map[string]interface{}{
				"notfound": "A1020",
			},
		},
		"delete": map[string]interface{}{
			"genre_id": map[string]interface{}{
				"invalid": "A1019",
			},
			"common_messages": map[string]interface{}{
				"notfound": "A1020",
			},
		},
	},
	"add_store_genre": map[string]interface{}{
		"post": map[string]interface{}{
			"genre_id": map[string]interface{}{
				"required": "A1016",
				"invalid":  "A1019",
			},
			"store_ingestion_spec_id": map[string]interface{}{
				"required": "A1016",
				"invalid":  "A1019"},
		},
	},
	"stores": map[string]interface{}{
		"delete": map[string]interface{}{
			"id": map[string]interface{}{
				"invalid": "A1021",
			},
		},
		"get": map[string]interface{}{
			"page": map[string]interface{}{
				"invalid_page": "A1022",
			},
			"limit": map[string]interface{}{
				"invalid_limit": "A1023",
				"maxlimit":      "A1024",
			},
			"sort": map[string]interface{}{
				"invalid_sort": "A1025",
			},
			"order": map[string]interface{}{
				"invalid_order": "A1026",
			},
			"status": map[string]interface{}{
				"invalid_status": "A1027",
			},
			"live": map[string]interface{}{
				"invalid_live": "A1028",
			},
		},
		"post": map[string]interface{}{
			"name": map[string]interface{}{
				"required": "A1029",
				"min":      "A1030",
				"max":      "A1031",
				"exists":   "A1032",
				"invalid":  "A1033",
			},
			"parent_id": map[string]interface{}{
				"max":     "A1034",
				"invalid": "A1035",
			},
			"variable_name": map[string]interface{}{
				"exists": "A1036",
			},
			"process_duration_days": map[string]interface{}{
				"max":     "A1037",
				"invalid": "A1038",
			},
			"remote_path": map[string]interface{}{
				"invalid": "A1039",
				"max":     "A1040",
			},
			"featurefm_name": map[string]interface{}{
				"invalid": "A1041",
				"max":     "A1042",
			},
			"ddex_version_id": map[string]interface{}{
				"invalid": "A1043",
			},
			"dolby_codec_id": map[string]interface{}{
				"max":     "A1044",
				"invalid": "A1045",
			},
			"logo": map[string]interface{}{
				"invalid": "A1046",
				"max":     "A1047",
			},
			"reporting_type": map[string]interface{}{
				"invalid": "A1048",
				"max":     "A1049",
			},
		},
		"patch": map[string]interface{}{
			"name": map[string]interface{}{
				"required": "A1050",
				"min":      "A1051",
				"max":      "A1052",
				"exists":   "A1053",
				"invalid":  "A1054",
			},
			"parent_id": map[string]interface{}{
				"max":     "A1055",
				"invalid": "A1056",
			},
			"variable_name": map[string]interface{}{
				"exists": "A1057",
			},
			"process_duration_days": map[string]interface{}{
				"max":     "A1058",
				"invalid": "A1059",
			},
			"remote_path": map[string]interface{}{
				"invalid": "A1060",
				"max":     "A1061",
			},
			"featurefm_name": map[string]interface{}{
				"invalid": "A1062",
				"max":     "A1063",
			},
			"ddex_version_id": map[string]interface{}{
				"invalid": "A1064",
			},
			"dolby_codec_id": map[string]interface{}{
				"max":     "A1065",
				"invalid": "A1066",
			},
			"logo": map[string]interface{}{
				"invalid": "A1067",
				"max":     "A1068",
			},
			"reporting_type": map[string]interface{}{
				"invalid": "A1069",
				"max":     "A1070",
			},
		},
	},
}
