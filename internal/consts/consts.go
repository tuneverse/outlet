package consts

const (
	// DatabaseType represents the type of the database being used.
	DatabaseType = "postgres"

	// AppName represents the name of the application.
	AppName = "store"

	// AcceptedVersions represents the accepted API versions.
	AcceptedVersions = "v1.0"
)

const (
	// ContextAcceptedVersions represents the context key for accepted versions.
	ContextAcceptedVersions = "Accept-Version"

	// ContextSystemAcceptedVersions represents the context key for system accepted versions.
	ContextSystemAcceptedVersions = "System-Accept-Versions"

	// ContextAcceptedVersionIndex represents the context key for accepted version index.
	ContextAcceptedVersionIndex = "Accepted-Version-index"

	// ContextEndPoints represents the context key for context endpoints.
	ContextEndPoints             = "context-endpoints"
	ContextLocallizationLanguage = "lan"
)

const (
	// HelpURL is the URL for help related to errors.
	HelpURL = "https://api.tv.com/help/error/#500"
)

const (
	// ContextErrorResponses represents the context key for error responses.
	ContextErrorResponses = "context-error-response"
	// ContextLocalizationLanguage represents the context key for localization language.
	ContextLocalizationLanguage = "lan"
)

const (
	// HeaderLocallizationLanguage represents the header key for localization language.
	HeaderLocallizationLanguage = "Accept-Language"
)

// CacheErrorData represents the cache name for error data.
const CacheErrorData = "CACHE_ERROR_DATA"

// ExpiryTime represents the expiry time for cache data in seconds.
const ExpiryTime = 180

// ValidationErrCode represents the error code for validation errors.
const ValidationErrCode = "400"

// KeyNames
const (
	Failure    = "Failure"
	SuccessKey = "Success"
	// ValidationErr represents the validation error string.
	ValidationErr = "validation_error"
	// ForbiddenErr represents the forbidden error string.
	ForbiddenErr = "forbidden"
	// UnauthorisedErr represents the unauthorized error string.
	UnauthorisedErr = "unauthorized"
	// NotFound represents the not found error string.
	NotFound = "not_found"
	// InternalServerErr represents the internal server error string.
	InternalServerErr = "internal_server_error"
	// Errors represents the generic error string.
	Errors = "errors"
	// AllError represents all errors string.
	AllError = "AllError"
	// Registration represents the registration string.
	Registration = "registration"
	// ErrorCode represents the error code string.
	ErrorCode = "errorCode"
	// MemberIDErr represents the member ID error string.
	MemberIDErr = "member_id"
	// UpdateStore represents the update store string.
	UpdateStore = "update_store"
	// AddStore represents the add store string.
	AddStore = "add_store"
)
const (
	// DefaultDateFormat is the default date format used.
	DefaultDateFormat = "02-01-2006"
	// DefaultPage is the default page number.
	DefaultPage = 1
	// DefaultLimit is the default limit for pagination.
	DefaultLimit = 10
	// MaxLimit is the maximum limit allowed for pagination.
	MaxLimit     = 50
	ShowAllLimit = 5000
)

const (
	EndpointErr = "Error occured while loading endpoints from service"
	ContextErr  = "Error occured while loading error from service"
	Success     = "Store added Successfully"
)

// s3 expiration
const (
	Expiration = 2
)

// consts used in utilities
const (
	LimitVal = 25
	InputOne = 10
	InputTwo = 64
)

const (
	Required = "required"
	Min      = "min"
	Max      = "max"
	Invalid  = "invalid"
	Exists   = "exists"
)

const (
	PartnerApiURL            = "http://10.1.0.90:8031/api/v1/partners"
	LookupApiURL             = "http://10.1.0.200:8080/api/v1.0/lookup"
	ReportingTypeId          = 22
	CacheEndpointsKey        = "context-endpoints"
	StoreBaseURL             = "http://10.1.0.108:8022/api/v1.0"
	SampleTuneVersePartnerId = "96b05633-69d7-4144-b0f9-73765b9ea7ca"
	SamplePartnerName        = "Hari"
	SampleStoreName          = "Sample_store"
	SampleStoreId            = "3706fcae-0697-45c2-b872-a9082c728df3"
	SampleStoreGenreId       = 25
	SampleStoreGenreName     = "Sample_genre"
	SampleStoreRoleId        = 140
	SampleStoreRoleName      = "Sample_role"
)
